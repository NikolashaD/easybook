<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsAmenitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('apartments_amenities', function(Blueprint $table){
                $table->increments('id')->unsigned();
                $table->integer('amenity_id')->unsigned()->index();
                $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
                $table->integer('apartment_id')->unsigned()->index();
                $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade');
                $table->timestamps();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('apartments_amenities');
	}
}
