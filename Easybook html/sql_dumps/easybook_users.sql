-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:15:56
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_country_id_foreign` (`country_id`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL,
  CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `telephone`, `image`, `remember_token`, `created_at`, `updated_at`, `country_id`, `role_id`) VALUES
	(1, 'Admin', 'Adminov', 'admin@gmail.com', '$2y$10$GD.JGwANjsbzx.TepbNuCuz6K2WUY8C4YLFTQVJ53Huqt2KdToi1u', '911', 'img/users/admin.png', NULL, '2015-01-28 11:05:57', '2015-01-28 11:05:57', 2, 1),
	(2, 'Dasha', 'Nikolaenko', 'nikolashaD@yandex.ru', '$2y$10$QuGpnMACWV.j5kj4Vk2xROi.kdfozx.ruFKCMOaEjC1QC53QBHfv6', '80299116609', 'img/users/2015_01_28_11_07_32_user.jpg', 'cetEVmOUwvsiYr425glkIp6wAX8NNCrbG4SZ6nN5OvVP4oDTQ1EVkVKyxabO', '2015-01-28 11:07:33', '2015-01-28 11:07:59', 2, 2),
	(3, 'Kate', 'Lavender', 'nikolasha@mail.ru', '$2y$10$1dDkcFbVcmKoy79JT/7tc.FOLjnHFPEspdK2ueYMcuLAvl/j3iONi', '80296659854', 'img/users/2015_01_28_11_09_06_загруженное.jpg', NULL, '2015-01-28 11:09:06', '2015-01-28 11:09:06', 9, 3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
