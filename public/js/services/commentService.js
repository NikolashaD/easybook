// public/js/services/commentService.js

angular.module('commentService', [])

.factory('Comment', function($http) {

    return {
        // get all the comments
        get : function(apartmentId) {
            $('textarea[name="content"]').val('');
            return $http.get('/main/apartments/'+apartmentId+'/comments');
        },

        // save a comment (pass in comment data)
        save : function(commentData) {
            return $http({
                method: 'POST',
                url: '/main/apartments/comments',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(commentData)
            });
        },

        // destroy a comment
        destroy : function(id) {
            return $http.delete('/main/apartments/comments/' + id);
        }
    }

});