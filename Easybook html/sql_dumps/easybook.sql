-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:13:38
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.amenities
DROP TABLE IF EXISTS `amenities`;
CREATE TABLE IF NOT EXISTS `amenities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.amenities: ~0 rows (approximately)
DELETE FROM `amenities`;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Wireless Internet', '2014-12-29 15:11:23', '2014-12-29 15:11:23'),
	(2, 'TV', '2014-12-29 15:11:47', '2014-12-29 15:11:47'),
	(3, 'Kitchen', '2014-12-29 15:11:55', '2014-12-29 15:11:55'),
	(4, 'Air Conditioning', '2014-12-29 15:12:04', '2014-12-29 15:12:04'),
	(5, 'Breakfast', '2014-12-29 15:12:08', '2014-12-29 15:12:08'),
	(6, 'Cable TV', '2014-12-29 15:12:11', '2014-12-29 15:12:11'),
	(7, 'Doorman', '2014-12-29 15:12:15', '2014-12-29 15:12:15'),
	(8, 'Dryer', '2014-12-29 15:12:19', '2014-12-29 15:12:19'),
	(9, 'Elevator in Building', '2014-12-29 15:12:23', '2014-12-29 15:12:23'),
	(10, 'Essentials', '2014-12-29 15:12:27', '2014-12-29 15:12:27'),
	(11, 'Family/Kid Friendly', '2014-12-29 15:12:31', '2014-12-29 15:12:31'),
	(12, 'Fire Extinguisher', '2014-12-29 15:12:34', '2014-12-29 15:12:34'),
	(13, 'First Aid Kit', '2014-12-29 15:12:38', '2014-12-29 15:12:38'),
	(14, 'Gym', '2014-12-29 15:12:43', '2014-12-29 15:12:43'),
	(15, 'Heating', '2014-12-29 15:12:46', '2014-12-29 15:12:46'),
	(16, 'Hot Tub', '2014-12-29 15:12:49', '2014-12-29 15:12:49'),
	(17, 'Indoor Fireplace', '2014-12-29 15:12:52', '2014-12-29 15:12:52'),
	(18, 'Internet', '2014-12-29 15:12:57', '2014-12-29 15:12:57'),
	(19, 'Pets Allowed', '2014-12-29 15:13:02', '2014-12-29 15:13:02'),
	(20, 'Pool', '2014-12-29 15:13:05', '2014-12-29 15:13:05'),
	(21, 'Safety Card', '2014-12-29 15:13:08', '2014-12-29 15:13:08'),
	(22, 'Shampoo', '2014-12-29 15:13:12', '2014-12-29 15:13:12'),
	(23, 'Smoke Detector', '2014-12-29 15:13:16', '2014-12-29 15:13:16'),
	(24, 'Smoking Allowed', '2014-12-29 15:13:19', '2014-12-29 15:13:19'),
	(25, 'Suitable for Events', '2014-12-29 15:13:23', '2014-12-29 15:13:23'),
	(26, 'Washer', '2014-12-29 15:13:27', '2014-12-29 15:13:27');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;


-- Dumping structure for table easybook.apartments
DROP TABLE IF EXISTS `apartments`;
CREATE TABLE IF NOT EXISTS `apartments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `beds` int(11) NOT NULL,
  `guests` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(10) unsigned DEFAULT NULL,
  `room_type_id` int(10) unsigned DEFAULT NULL,
  `property_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apartments_country_id_foreign` (`country_id`),
  KEY `apartments_room_type_id_foreign` (`room_type_id`),
  KEY `apartments_property_type_id_foreign` (`property_type_id`),
  CONSTRAINT `apartments_property_type_id_foreign` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`) ON DELETE SET NULL,
  CONSTRAINT `apartments_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `apartments_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments: ~0 rows (approximately)
DELETE FROM `apartments`;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
INSERT INTO `apartments` (`id`, `name`, `description`, `price`, `bedrooms`, `bathrooms`, `beds`, `guests`, `created_at`, `updated_at`, `country_id`, `room_type_id`, `property_type_id`) VALUES
	(1, 'First Apartment', 'Apartment (for 2 people) \r\nA room with kitchen and balcony (TV, fridge, wireless Internet), bathroom with shower and WC.The apartment has direct access to the balcony.', 3000.00, 2, 2, 2, 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42', 2, 2, 1),
	(2, 'Second Apartment', 'Apartment (for 4+1 people)\r\n2 double bedrooms with balcony (TV, fridge, wireless Internet), 2 bathrooms with shower and WC, kitchen.\r\nThe apartment has direct access to the balcony and a view of the village and the surrounding mountains.', 2000.00, 2, 0, 2, 3, '2015-01-28 11:02:55', '2015-01-28 11:02:55', 14, 2, 1);
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;


-- Dumping structure for table easybook.apartments_amenities
DROP TABLE IF EXISTS `apartments_amenities`;
CREATE TABLE IF NOT EXISTS `apartments_amenities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amenity_id` int(10) unsigned NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartments_amenities_amenity_id_index` (`amenity_id`),
  KEY `apartments_amenities_apartment_id_index` (`apartment_id`),
  CONSTRAINT `apartments_amenities_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `apartments_amenities_amenity_id_foreign` FOREIGN KEY (`amenity_id`) REFERENCES `amenities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments_amenities: ~0 rows (approximately)
DELETE FROM `apartments_amenities`;
/*!40000 ALTER TABLE `apartments_amenities` DISABLE KEYS */;
INSERT INTO `apartments_amenities` (`id`, `amenity_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 7, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 10, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 14, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 17, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 20, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 26, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 5, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 8, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 10, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 19, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_amenities` ENABLE KEYS */;


-- Dumping structure for table easybook.apartments_hostlanguages
DROP TABLE IF EXISTS `apartments_hostlanguages`;
CREATE TABLE IF NOT EXISTS `apartments_hostlanguages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostlanguage_id` int(10) unsigned NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartments_hostlanguages_hostlanguage_id_index` (`hostlanguage_id`),
  KEY `apartments_hostlanguages_apartment_id_index` (`apartment_id`),
  CONSTRAINT `apartments_hostlanguages_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `apartments_hostlanguages_hostlanguage_id_foreign` FOREIGN KEY (`hostlanguage_id`) REFERENCES `host_languages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments_hostlanguages: ~0 rows (approximately)
DELETE FROM `apartments_hostlanguages`;
/*!40000 ALTER TABLE `apartments_hostlanguages` DISABLE KEYS */;
INSERT INTO `apartments_hostlanguages` (`id`, `hostlanguage_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 22, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 22, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_hostlanguages` ENABLE KEYS */;


-- Dumping structure for table easybook.apartment_calendars
DROP TABLE IF EXISTS `apartment_calendars`;
CREATE TABLE IF NOT EXISTS `apartment_calendars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apartment_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartment_calendars_apartment_id_foreign` (`apartment_id`),
  CONSTRAINT `apartment_calendars_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartment_calendars: ~0 rows (approximately)
DELETE FROM `apartment_calendars`;
/*!40000 ALTER TABLE `apartment_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `apartment_calendars` ENABLE KEYS */;


-- Dumping structure for table easybook.apartment_images
DROP TABLE IF EXISTS `apartment_images`;
CREATE TABLE IF NOT EXISTS `apartment_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartment_images_apartment_id_foreign` (`apartment_id`),
  CONSTRAINT `apartment_images_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartment_images: ~0 rows (approximately)
DELETE FROM `apartment_images`;
/*!40000 ALTER TABLE `apartment_images` DISABLE KEYS */;
INSERT INTO `apartment_images` (`id`, `image`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 'img/apartments/2015_01_28_11_01_42_pic-1.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(2, 'img/apartments/2015_01_28_11_01_42_pic-2.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(3, 'img/apartments/2015_01_28_11_01_42_pic-3.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(4, 'img/apartments/2015_01_28_11_02_55_pic-1.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(5, 'img/apartments/2015_01_28_11_02_55_pic-2.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(6, 'img/apartments/2015_01_28_11_02_55_pic-3 - Copy.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(7, 'img/apartments/2015_01_28_11_02_55_pic-3.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55');
/*!40000 ALTER TABLE `apartment_images` ENABLE KEYS */;


-- Dumping structure for table easybook.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.countries: ~0 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Check Republic', 'Check', 'img/countries/2015_01_28_10_47_11_check.png', '2015-01-28 10:44:36', '2015-01-28 10:47:11'),
	(2, 'Belarus', '', 'img/countries/2015_01_28_10_47_19_belarus.png', '2015-01-28 10:45:31', '2015-01-28 10:47:19'),
	(3, 'China', '', 'img/countries/2015_01_28_10_47_31_china.png', '2015-01-28 10:47:31', '2015-01-28 10:47:31'),
	(4, 'Egypt', '', 'img/countries/2015_01_28_10_47_45_egypt.png', '2015-01-28 10:47:45', '2015-01-28 10:47:45'),
	(5, 'England', '', 'img/countries/2015_01_28_10_47_54_england.png', '2015-01-28 10:47:54', '2015-01-28 10:47:54'),
	(6, 'Finland', '', 'img/countries/2015_01_28_10_48_07_finland.png', '2015-01-28 10:48:08', '2015-01-28 10:48:08'),
	(7, 'France', '', 'img/countries/2015_01_28_10_48_15_france.png', '2015-01-28 10:48:16', '2015-01-28 10:48:16'),
	(8, 'Germany', '', 'img/countries/2015_01_28_10_48_28_germany.png', '2015-01-28 10:48:28', '2015-01-28 10:48:28'),
	(9, 'Italy', '', 'img/countries/2015_01_28_10_48_43_italy.png', '2015-01-28 10:48:43', '2015-01-28 10:48:43'),
	(10, 'Japan', '', 'img/countries/2015_01_28_10_49_09_japan.png', '2015-01-28 10:49:10', '2015-01-28 10:49:10'),
	(11, 'Netherlands', '', 'img/countries/2015_01_28_10_49_23_netherlands.png', '2015-01-28 10:49:23', '2015-01-28 10:49:23'),
	(13, 'Poland', '', 'img/countries/2015_01_28_10_49_54_poland.png', '2015-01-28 10:49:54', '2015-01-28 10:49:54'),
	(14, 'Russia', '', 'img/countries/2015_01_28_10_50_14_russia.png', '2015-01-28 10:50:14', '2015-01-28 10:50:14'),
	(15, 'USA', '', 'img/countries/2015_01_28_10_50_25_usa.png', '2015-01-28 10:50:26', '2015-01-28 10:50:26');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


-- Dumping structure for table easybook.host_languages
DROP TABLE IF EXISTS `host_languages`;
CREATE TABLE IF NOT EXISTS `host_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.host_languages: ~0 rows (approximately)
DELETE FROM `host_languages`;
/*!40000 ALTER TABLE `host_languages` DISABLE KEYS */;
INSERT INTO `host_languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(2, 'English', '2014-12-29 15:56:02', '2014-12-29 15:56:02'),
	(3, 'Español', '2014-12-29 15:56:05', '2014-12-29 15:56:05'),
	(4, 'Français', '2014-12-29 15:56:09', '2014-12-29 15:56:09'),
	(5, 'Dansk', '2014-12-29 15:56:13', '2014-12-29 15:56:13'),
	(6, 'Bengali', '2014-12-29 15:56:16', '2014-12-29 15:56:16'),
	(7, 'Deutsch', '2014-12-29 15:56:20', '2014-12-29 15:56:20'),
	(8, 'Hindi', '2014-12-29 15:56:23', '2014-12-29 15:56:23'),
	(9, 'Italiano', '2014-12-29 15:56:26', '2014-12-29 15:56:26'),
	(10, 'Magyar', '2014-12-29 15:56:30', '2014-12-29 15:56:30'),
	(11, 'Nederlands', '2014-12-29 15:56:33', '2014-12-29 15:56:33'),
	(12, 'Norsk', '2014-12-29 15:56:36', '2014-12-29 15:56:36'),
	(13, 'Polski', '2014-12-29 15:56:40', '2014-12-29 15:56:40'),
	(14, 'Português', '2014-12-29 15:56:44', '2014-12-29 15:56:44'),
	(15, 'Punjabi', '2014-12-29 15:56:47', '2014-12-29 15:56:47'),
	(16, 'Suomi', '2014-12-29 15:56:51', '2014-12-29 15:56:51'),
	(17, 'Svenska', '2014-12-29 15:56:54', '2014-12-29 15:56:54'),
	(18, 'Tagalog', '2014-12-29 15:56:57', '2014-12-29 15:56:57'),
	(19, 'Türkçe', '2014-12-29 15:57:00', '2014-12-29 15:57:00'),
	(20, 'Čeština', '2014-12-29 15:57:03', '2014-12-29 15:57:03'),
	(21, 'Ελληνικά', '2014-12-29 15:57:07', '2014-12-29 15:57:07'),
	(22, 'Русский', '2014-12-29 15:57:36', '2014-12-29 15:57:36'),
	(23, 'українська', '2014-12-29 15:57:40', '2014-12-29 15:57:40'),
	(24, 'עברית', '2014-12-29 15:57:48', '2014-12-29 15:57:48'),
	(25, 'العربية', '2014-12-29 15:57:51', '2014-12-29 15:57:51'),
	(26, 'ภาษาไทย', '2014-12-29 15:57:54', '2014-12-29 15:57:54'),
	(27, '中文', '2014-12-29 15:57:58', '2014-12-29 15:57:58'),
	(28, '日本語', '2014-12-29 15:58:01', '2014-12-29 15:58:01'),
	(29, '한국어', '2014-12-29 15:58:04', '2014-12-29 15:58:04');
/*!40000 ALTER TABLE `host_languages` ENABLE KEYS */;


-- Dumping structure for table easybook.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.migrations: ~12 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_12_23_113405_create_countries_table', 1),
	('2014_12_29_152442_create_roomTypes_table', 1),
	('2014_12_29_154150_create_propertyTypes_table', 1),
	('2014_12_29_155443_create_hostLanguages_table', 1),
	('2014_12_31_132135_create_apartments_table', 1),
	('2014_12_31_132147_create_amenities_table', 1),
	('2014_12_31_133349_create_apartments_amenities_table', 1),
	('2015_01_05_095336_create_apartments_hostlanguages_table', 1),
	('2015_01_06_121652_create_apartment_images_table', 1),
	('2015_01_23_144846_create_apartment_calendars_table', 1),
	('2015_01_27_110026_create_roles_table', 1),
	('2015_01_27_110055_create_users_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table easybook.property_types
DROP TABLE IF EXISTS `property_types`;
CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.property_types: ~0 rows (approximately)
DELETE FROM `property_types`;
/*!40000 ALTER TABLE `property_types` DISABLE KEYS */;
INSERT INTO `property_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', '2014-12-29 15:45:07', '2014-12-29 15:45:07'),
	(2, 'House', '2014-12-29 15:45:12', '2014-12-29 15:45:12'),
	(3, 'Bed & Breakfast', '2014-12-29 15:45:21', '2014-12-29 15:45:21'),
	(4, 'Boat', '2014-12-29 15:45:30', '2014-12-29 15:45:30'),
	(5, 'Cabin', '2014-12-29 15:45:33', '2014-12-29 15:45:33'),
	(6, 'Camper/RV', '2014-12-29 15:45:36', '2014-12-29 15:45:36'),
	(7, 'Car', '2014-12-29 15:45:40', '2014-12-29 15:45:40'),
	(8, 'Castle', '2014-12-29 15:45:43', '2014-12-29 15:45:43'),
	(9, 'Cave', '2014-12-29 15:45:48', '2014-12-29 15:45:48'),
	(10, 'Chalet', '2014-12-29 15:45:53', '2014-12-29 15:45:53'),
	(11, 'Dorm', '2014-12-29 15:45:56', '2014-12-29 15:45:56'),
	(12, 'Earth House', '2014-12-29 15:46:00', '2014-12-29 15:46:00'),
	(13, 'Hut', '2014-12-29 15:46:04', '2014-12-29 15:46:04'),
	(14, 'Igloo', '2014-12-29 15:46:08', '2014-12-29 15:46:08'),
	(15, 'Island', '2014-12-29 15:46:11', '2014-12-29 15:46:11'),
	(16, 'Lighthouse', '2014-12-29 15:46:15', '2014-12-29 15:46:15'),
	(17, 'Loft', '2014-12-29 15:46:18', '2014-12-29 15:46:18'),
	(18, 'Other...', '2014-12-29 15:46:22', '2014-12-29 15:46:22');
/*!40000 ALTER TABLE `property_types` ENABLE KEYS */;


-- Dumping structure for table easybook.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nicename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.roles: ~0 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `nicename`, `created_at`, `updated_at`) VALUES
	(1, 'SUPER_ADMIN', 'Administrator', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'LANDLORD', 'Landlord', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'TENANT', 'Tenant', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table easybook.room_types
DROP TABLE IF EXISTS `room_types`;
CREATE TABLE IF NOT EXISTS `room_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.room_types: ~0 rows (approximately)
DELETE FROM `room_types`;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Entire Place', '2014-12-29 15:35:44', '2014-12-29 15:35:44'),
	(2, 'Private Room', '2014-12-29 15:35:59', '2014-12-29 15:35:59'),
	(3, 'Shared Room', '2014-12-29 15:36:11', '2014-12-29 15:36:11');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;


-- Dumping structure for table easybook.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_country_id_foreign` (`country_id`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL,
  CONSTRAINT `users_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `telephone`, `image`, `remember_token`, `created_at`, `updated_at`, `country_id`, `role_id`) VALUES
	(1, 'Admin', 'Adminov', 'admin@gmail.com', '$2y$10$GD.JGwANjsbzx.TepbNuCuz6K2WUY8C4YLFTQVJ53Huqt2KdToi1u', '911', 'img/users/admin.png', NULL, '2015-01-28 11:05:57', '2015-01-28 11:05:57', 2, 1),
	(2, 'Dasha', 'Nikolaenko', 'nikolashaD@yandex.ru', '$2y$10$QuGpnMACWV.j5kj4Vk2xROi.kdfozx.ruFKCMOaEjC1QC53QBHfv6', '80299116609', 'img/users/2015_01_28_11_07_32_user.jpg', 'cetEVmOUwvsiYr425glkIp6wAX8NNCrbG4SZ6nN5OvVP4oDTQ1EVkVKyxabO', '2015-01-28 11:07:33', '2015-01-28 11:07:59', 2, 2),
	(3, 'Kate', 'Lavender', 'nikolasha@mail.ru', '$2y$10$1dDkcFbVcmKoy79JT/7tc.FOLjnHFPEspdK2ueYMcuLAvl/j3iONi', '80296659854', 'img/users/2015_01_28_11_09_06_загруженное.jpg', NULL, '2015-01-28 11:09:06', '2015-01-28 11:09:06', 9, 3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
