<?php 

class WishlistController extends BaseController{

    public function __construct(){
        //$this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
    }

    public function addToWishlist(){
        $wishlist = new Wishlist();

        $wishlist->apartment_id = Input::get('apartment_id');
        $wishlist->user_id = Input::get('user_id');
        $wishlist->save();

        return Response::json(array('success' => true));
    }   

    public function removeFromWishlist(){
        $wishlist = Wishlist::query();

        $wishlist->where('user_id', Input::get('user_id'))
        		 ->where('apartment_id', Input::get('apartment_id'));

        $wishlist->delete();
        
        return Response::json(array('success' => true));
    }      


    public function getIndex() {
        $apartments = array();        
        foreach(Auth::user()->wishlist as $key=>$wish){
        	$apartment = Apartment::find($wish->apartment_id);
            $apartments[$key] = $apartment;
        }
       
        return View::make('wishlist.index')
        		->with('apartments', $apartments)
                ->with('amenities', Amenity::all())
                ->with('hostLanguages', HostLanguage::all())        		 
        ;
    }  

    public function postDestroy() {
        $wishlist = Wishlist::query();

        $wishlist->where('user_id', Input::get('user_id'))
        		 ->where('apartment_id', Input::get('apartment_id'));
        		 
		if($wishlist){
        	$wishlist->delete();
            
            return Redirect::to('admin/wishlist/index')
                ->with('message', 'Apartment Deleted From Wish List!');
        }

        return Redirect::to('admin/wishlist/index')
            ->with('message', 'Something went wrong, please, try again');
    }
}