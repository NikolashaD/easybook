@extends('layouts.dashboard')

@section('content')
  <h1>Calendars Admin Panel</h1>

  <p>Here you can view, delete, and create new reservations.</p>
  <hr />
  <div class="pre-datepicker">
      <a href="#" class="open-popup btn btn-success"><i class="glyphicon glyphicon-plus"></i> Add new reservation</a>  
      <table style="float:right;" class="table info">
        <tr>
          <td><p style="color:#4cae4c;"><i style="font-size:20px;" class="glyphicon glyphicon-ok-circle"></i></p></td>
          <td> - Avaliable</td>
          <td><p style="color:#ac2925;"><i style="font-size:20px;" class="glyphicon glyphicon-remove-circle"></i></p> </td>
          <td> - Booked</td>        
        </tr>
        <tr>
          <td><p style="color:#eea236;"><i style="font-size:20px;" class="glyphicon glyphicon-record"></i></p> </td>
          <td> - Special</td>
          <td><p style="color:#204d74;"><i style="font-size:20px;" class="glyphicon glyphicon-ban-circle"></i></p> </td>
          <td> - Unavaliable</td>
        </tr>
      </table>
  </div>
  <br />


  <div id="light" class="white_content"> 
  <b>Reservation information</b>
  <a class="close-popup" href="#"><i class="glyphicon glyphicon-remove"></i></a>

  {{ Form::open(array('url'=>'admin/calendars/create'))}}
  <div class="date">
    <table class="table">
      <tr>
          <td>Date</td>
          <td>{{ Form::text('date', '', array('class'=>'tcal form-control')) }}</td>
      </tr>
      <tr>
          <td>Start Date</td>
          <td>{{ Form::text('from', '', array('class'=>'tcal form-control')) }}</td>
      </tr>
      <tr>
          <td>End Date</td>
          <td>{{ Form::text('to', '', array('class'=>'tcal form-control')) }}</td>
      </tr>
    </table>
  </div>

  <div class="information">
    <table class="table">
      <tr>
          <td>Apartment</td>
          <td>{{ Form::select('apartment_id', $apartmentsNames ,'', array('class'=>'form-control')) }}</td>
      </tr>
      <tr>
          <td>Status</td>
          <td>{{ Form::select('status', array('Avaliable', 'Booked', 'Special', 'Unavaliable') ,'', array('class'=>'form-control')) }}</td>
      </tr>      
      <tr>
          <td colspan="2">Information (info about reservation)</td>
      </tr> 
      <tr>
          <td colspan="2">{{ Form::textarea('information', '', array('class'=>'form-control', 'rows'=>'2', 'style'=>'resize:none')) }}</td>
      </tr>       
      <tr>
          <td colspan="2">Notes (only you will see this message)</td>
      </tr> 
      <tr>
          <td colspan="2">{{ Form::textarea('note', '', array('class'=>'form-control', 'rows'=>'2', 'style'=>'resize:none')) }}</td>
      </tr> 
      <tr>
          <td colspan="2"><div style="float:right;">{{ Form::submit('Create', array('class'=>'btn btn-success')) }}</div></td>
      </tr>         
    </table>    
  </div>
</div>
{{ Form::close() }}
<div id="fade" class="black_overlay"></div>    

  
@foreach($apartments as $apartment)
  <h3>{{ $apartment->name }}</h3>
  <table class="table">
      <thead>
          <tr>
              <th width="5%">№</th>
              <th width="30%">Information</th>
              <th width="30%">Note</th>
              <th width="10%">Date</th>
              <th width="10%">From</th>
              <th width="10%">To</th>
              <th width="10%">Status</th>
              <th>Action</th>
          </tr>
      </thead>

      <tbody>          
          @foreach($apartment->calendars as $key=>$calendar)
          {{ Form::open(array('url'=>'admin/calendars/destroy', 'files'=>true)) }}
            <tr class="show-grey-simple">
                <td>{{ $key+1 }}</td>
                <td>{{ $calendar->information }}</td>
                <td>{{ $calendar->note }}</td>
                <td>{{ $calendar->date }}</td>
                <td>{{ $calendar->from }}</td>
                <td>{{ $calendar->to }}</td>
                <td>
                  @if ($calendar->status == 0)
                      <p style="color:#4cae4c;"><i style="font-size:30px;" class="glyphicon glyphicon-ok-circle"></i></p>
                  @elseif ($calendar->status == 1)
                      <p style="color:#ac2925;"><i style="font-size:30px;" class="glyphicon glyphicon-remove-circle"></i></p>
                  @elseif ($calendar->status == 2)
                      <p style="color:#eea236;"><i style="font-size:30px;" class="glyphicon glyphicon-record"></i></p>    
                  @elseif ($calendar->status == 3)
                      <p style="color:#204d74;"><i style="font-size:30px;" class="glyphicon glyphicon-ban-circle"></i></p>    
                  @else
                      <p>Unknown</p> 
                  @endif
                </td>
                <td>
                    {{ Form::hidden('id', $calendar->id) }}
                    {{ Form::submit('delete', array('class'=>'btn btn-danger')) }}
                </td>
            </tr>
          {{ Form::close() }} 
            <tr class="hide show-white">
                <td>{{ Form::open(array('url'=>'admin/calendars/update', 'class'=>'form-inline')) }}</td>
                <td>{{ Form::textarea('information', $calendar->information, array('class'=>'form-control', 'rows'=>'2')) }}</td>
                <td>{{ Form::textarea('note', $calendar->note, array('class'=>'form-control', 'rows'=>'2')) }}</td>
                <td>{{ Form::text('date', $calendar->date, array('class'=>'tcal form-control')) }}</td>
                <td>{{ Form::text('from', $calendar->from, array('class'=>'tcal form-control')) }}</td>
                <td>{{ Form::text('to', $calendar->to, array('class'=>'tcal form-control')) }}</td>
                <td>{{ Form::select('status', array('Avaliable', 'Booked', 'Special', 'Unavaliable'), 0, array('class'=>'form-control')) }}</td>
                <td>                        
                    {{ Form::hidden('id', $calendar->id) }}
                    {{ Form::hidden('apartment_id', $apartment->id) }}
                    {{ Form::submit('save', array('class'=>'btn btn-success')) }}
                    {{ Form::close() }}
                </td>
            </tr>             

          @endforeach                  
      </tbody>
  </table>
  <br /><br /><br />
  @endforeach

@stop