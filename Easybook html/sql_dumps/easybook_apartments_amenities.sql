-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:14:10
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.apartments_amenities
DROP TABLE IF EXISTS `apartments_amenities`;
CREATE TABLE IF NOT EXISTS `apartments_amenities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amenity_id` int(10) unsigned NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartments_amenities_amenity_id_index` (`amenity_id`),
  KEY `apartments_amenities_apartment_id_index` (`apartment_id`),
  CONSTRAINT `apartments_amenities_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `apartments_amenities_amenity_id_foreign` FOREIGN KEY (`amenity_id`) REFERENCES `amenities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments_amenities: ~13 rows (approximately)
DELETE FROM `apartments_amenities`;
/*!40000 ALTER TABLE `apartments_amenities` DISABLE KEYS */;
INSERT INTO `apartments_amenities` (`id`, `amenity_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 7, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 10, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 14, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 17, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 20, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 26, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 5, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 8, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 10, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 19, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_amenities` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
