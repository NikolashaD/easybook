<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('calendars')){		
	        Schema::create('calendars', function($table){
	                $table->increments('id');
	                $table->integer('apartment_id')->unsigned();
	                $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade')->onUpdate('cascade');
	                $table->integer('status');
	                $table->date('date')->nullable();
	                $table->date('from')->nullable();
	                $table->date('to')->nullable();
	                $table->string('information');
	                $table->string('note');

	                $table->timestamps();
	            });
	    }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendars');
	}

}
