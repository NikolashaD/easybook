<?php

class PropertyTypesController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/propertyTypes');          
    }

    public function getIndex() {
        return View::make('propertyTypes.index')
                    ->with('propertyTypes', PropertyType::all());
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), PropertyType::$rules);

        if($validator->passes()){
            $propertyType = new PropertyType();
            $this->savePropertyType($propertyType);

            return Redirect::to('admin/propertyTypes/index')
                        ->with('message', 'New property type has been successfully created!');
        }

        return Redirect::to('admin/propertyTypes/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }

    
    public function postUpdate() {
        $propertyType = PropertyType::find(Input::get('id'));

        if($propertyType){
            $this->savePropertyType($propertyType);
            
            return Redirect::to('admin/propertyTypes/index')
                ->with('message', 'Property Type Updated!');
        }

        return Redirect::to('admin/propertyTypes/index')
            ->with('message', 'Something went wrong, please, try again');
    }    
    
    
    public function postDestroy() {
        $propertyType = PropertyType::find(Input::get('id'));

        if($propertyType){
            $propertyType->delete();

            return Redirect::to('admin/propertyTypes/index')
                ->with('message', 'Property Type Deleted!');
        }

        return Redirect::to('admin/propertyTypes/index')
            ->with('message', 'Something went wrong, please, try again');
    }



    private function savePropertyType($propertyType){
        $propertyType->name = Input::get('name');
        if($propertyType->save()){
            return true;
        }

        return false;
    }

}