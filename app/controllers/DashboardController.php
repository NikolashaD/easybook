<?php

class DashboardController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
    }

    public function getIndex() {
        return View::make('dashboard.index');
    }
}