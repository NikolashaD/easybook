<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('apartments', function($table){
                $table->increments('id');
                $table->string('name');
                $table->string('description', 2000);
                $table->decimal('price', 6, 2);
                $table->integer('bedrooms');
                $table->integer('bathrooms');
                $table->integer('beds');
                $table->integer('guests');
                $table->string('city');
                $table->string('address');
                $table->string('longitude');
                $table->string('latitude');
                $table->timestamps();  
//                $table->string('image');                
                
                $table->integer('country_id')->unsigned()->nullable();
                $table->integer('room_type_id')->unsigned()->nullable();
                $table->integer('property_type_id')->unsigned()->nullable();
                $table->integer('user_id')->unsigned();                
#                $table->integer('amenity_id')->unsigned()->nullable();                
#                $table->integer('host_language_id')->unsigned()->nullable();
            });
            
            
            Schema::table('apartments', function($table){                
                $table->foreign('country_id')->references('id')->on('countries')->onDelete(DB::raw('set null'));             
                $table->foreign('room_type_id')->references('id')->on('room_types')->onDelete(DB::raw('set null'));
                $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete(DB::raw('set null'));  
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');                   
#                $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete(DB::raw('set null'));                              
#                $table->foreign('host_language_id')->references('id')->on('host_languages')->onDelete(DB::raw('set null'));                  
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //Schema::drop('apartments');
        Schema::table('apartments', function(Blueprint $table) {
            $table->dropForeign('country_id');
            $table->dropForeign('room_type_id');
            $table->dropForeign('property_type_id');
            $table->dropForeign('user_id');
        });        
	}

}
