@extends('layouts.dashboard')

@section('content')
  <h1>Wish List Admin Panel</h1>

  <p>Here you can view and delete apartments, that you've liked.</p>
  <hr />  

    <br /><br />
    <h2>Apartments</h2>
    <hr />

    <table class="table">
        <thead>
            <tr>
                <th width="5%">№</th>
                <th width="10%">Image</th>
                <th width="25%">Title</th>
                <th width="25%">Country</th>
                <th width="25%">Price $</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($apartments as $key=>$apartment)
            <div class="hide"> 
                @foreach($apartment->amenities as $ka=>$a)
                    {{ $apartmentAmenities[$ka] = $a->id; }}
                @endforeach
                <br />
                @foreach($apartment->hostLanguages as $kh=>$h)
                    {{ $apartmentHostlanguages[$kh] = $h->id; }}
                @endforeach  
                <br /><br />
            </div>
            <tr class="show-grey">
                <td>
                    {{ $key + 1 }}
                </td>
                <td>
                    @if(isset($apartment->apartmentImages[0]))
                        {{ HTML::image($apartment->apartmentImages[0]->image, $apartment->name, array('width'=>'100')) }}
                    @endif
                </td>                
                <td>
                    {{ $apartment->name }}
                </td>
                <td>
                    @if($apartment->country)                    
                        {{ $apartment->country->name }}
                    @endif                         
                </td>            
                <td>                 
                    {{ $apartment->price }} $                  
                </td>                
                <td>                    
                    {{ Form::open(array('url'=>'admin/wishlist/destroy', 'class'=>'form-inline')) }}
                    {{ Form::hidden('apartment_id', $apartment->id) }}
                    {{ Form::hidden('user_id', Auth::user()->id) }}
                    {{ HTML::link('main/apartments/'.$apartment->id.'','', array('class'=>'glyphicon glyphicon-search btn btn-info'))}}
                    {{ Form::submit('delete', array('class'=>'btn btn-danger')) }}                                        
                    {{ Form::close() }}
                </td>
            </tr>

                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Info') }}</td>                  
                    <td colspan="2">{{ Form::label('Title') }} {{ Form::label($apartment->name)}}</td> 
                    <td>{{ Form::label('Price') }} {{ $apartment->price }}</td>    
                    <td></td>                 
                </tr>             
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Description') }}</td>                  
                    <td colspan="3">{{ $apartment->description }}</td> 
                    <td></td>                 
                </tr>       
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Location') }}</td>                  
                    <td>{{ Form::label('Country') }}  {{ $apartment->country->name}}
                    <td>{{ Form::label('Room type') }} {{ $apartment->roomType->name }}</td>                    
                    <td>{{ Form::label('Property type') }} {{ $apartment->propertyType->name }}</td>
                    <td></td>                 
                </tr>  
                <tr class="hide">
                    <td></td>                
                    <td></td>   
                    <td>{{ Form::label('City') }} {{ $apartment->city }}</td>       
                    <td colspan="2">{{ Form::label('Address') }} {{ $apartment->address }}</td>                    
                    <td></td>                 
                </tr>                 
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Size') }}</td>                  
                    <td>{{ Form::label('Bedrooms') }} {{ $apartment->bedrooms }}</td>
                    <td>{{ Form::label('Bathrooms') }} {{ $apartment->bathrooms }}</td>
                    <td>{{ Form::label('Beds') }} {{ $apartment->beds }}</td>
                    <td></td>                 
                </tr> 
                    @for ($i = 0; $i < count($amenities); $i+=3)     
                    <tr class="hide">
                        <td></td>
                        <td>
                            @if($i == 0)
                                {{ Form::label('Amenities') }}
                            @endif
                        </td>
                        <td>  
                            @if(isset($amenities[$i]))
                                {{ Form::checkbox('amenities[]', $amenities[$i]->id, in_array($amenities[$i]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                {{ $amenities[$i]->name }} 
                            @endif
                        </td>                        
                        <td>     
                            @if(isset($amenities[$i+1]))
                                {{ Form::checkbox('amenities[]', $amenities[$i+1]->id, in_array($amenities[$i+1]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                {{ $amenities[$i+1]->name }} 
                            @endif
                        </td>
                        <td>    
                            @if(isset($amenities[$i+2]))
                                {{ Form::checkbox('amenities[]', $amenities[$i+2]->id, in_array($amenities[$i+2]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                {{ $amenities[$i+2]->name }} 
                            @endif
                        </td> 
                        <td></td>  
                    </tr>   
                    @endfor   

                    @for ($i = 0; $i < count($hostLanguages); $i+=3)                 
                    <tr class="hide">
                        <td></td> 
                        <td>
                            @if($i == 0)
                                {{ Form::label('Host languages') }}
                            @endif
                        </td>
                        <td>  
                            @if(isset($hostLanguages[$i]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i]->id, in_array($hostLanguages[$i]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                {{ $hostLanguages[$i]->name }} 
                            @endif
                        </td>                        
                        <td>     
                            @if(isset($hostLanguages[$i+1]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+1]->id, in_array($hostLanguages[$i+1]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                {{ $hostLanguages[$i+1]->name }} 
                            @endif
                        </td>
                        <td>    
                            @if(isset($hostLanguages[$i+2]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+2]->id, in_array($hostLanguages[$i+2]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                {{ $hostLanguages[$i+2]->name }} 
                            @endif
                        </td>
                        <td></td> 
                    </tr>   
                    @endfor        
                    <tr class="hide">
                        <td></td>
                        <td>{{ Form::label('Image', 'Image') }}</td>
                        <td>
                            @foreach($apartment->apartmentImages as $image)
                            <div style="float:left; padding:5px;">{{ HTML::image($image->image, '', array('width'=>'120',)) }}</div>
                            @endforeach
                        </td>
                        <td></td>
                        <td>{{ Form::label('Guests') }} {{ $apartment->guests}}</td>
                        <td> 
                        </td>
                    </tr> 
                    <tr class="hide">
                        <td colspan="6"></td>
                    </tr>                  
                {{ Form::close() }}

            @endforeach            
        </tbody>
    </table>  

@stop  