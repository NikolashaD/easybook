<?php

class CountriesController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/countries');        
    }

    public function getIndex() {
        return View::make('countries.index')
                    ->with('countries', Country::all());
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), Country::$rules);

        if($validator->passes()){
            $country = new Country();
            $country->name = Input::get('name');
            $country->description = Input::get('description');
            
            $image = Input::file('image');   
            $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
            $path = public_path('img/countries/' . $filename);
            Image::make($image->getRealPath())->resize(468,249)->save($path);
            
            $country->image = 'img/countries/'.$filename;          
            $country->save();

            return Redirect::to('admin/countries/index')
                        ->with('message', 'New country has been successfully created!');
        }

        return Redirect::to('admin/countries/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $country = Country::find(Input::get('id'));

        if($country){
            $country->name = Input::get('name');
            $country->description = Input::get('description');     

            $image = Input::file('image');
            if($image){
                $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
                $path = public_path('img/countries/' . $filename);
                Image::make($image->getRealPath())->resize(468,249)->save($path);

                $country->image = 'img/countries/'.$filename;  
            }      
            $country->save();    
            
            return Redirect::to('admin/countries/index')
                ->with('message', 'Country Updated!');
        }

        return Redirect::to('admin/countries/index')
            ->with('message', 'Something went wrong, please, try again');
    }     

    public function postDestroy() {
        $country = Country::find(Input::get('id'));

        if($country){            
            File::delete('public/'.$country->image);
            $country->delete();

            return Redirect::to('admin/countries/index')
                ->with('message', 'Country Deleted!');
        }

        return Redirect::to('admin/countries/index')
            ->with('message', 'Something went wrong, please, try again');
    }
    
    public function getDeleteSelected() {
        $ids = Input::get('ids');
        
        if($ids){
            foreach($ids as $id){
                $country = Country::find($id);
                if($country){
                    File::delete('public/'.$country->image);
                    $country->delete();                     
                }
            }
            
            return Response::json(array('success' => true));
        }  

        return Response::json(array('success' => false));
    }    

}