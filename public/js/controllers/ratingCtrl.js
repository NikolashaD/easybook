angular.module('ratingCtrl', [])

.controller('ratingController', function($scope, $http, Rating) {
    $scope.ratingData = {};
    $scope.loading = true;
    $scope.list = [1,2,3,4,5];

    Rating.get(apartmentId)
        .success(function(data) {
            console.log(data);
            $scope.rating = data;
            $scope.loading = false;
        });

    $scope.submitRating = function() {
        $scope.loading = true;

        Rating.save($scope.ratingData)
            .success(function(data) {
                Rating.get(apartmentId)
                    .success(function(getData) {
                        console.log(getData);
                        $scope.rating = getData;
                        $scope.loading = false;
                    });

            })
            .error(function(data) {
                console.log(data);
            });
    };

});