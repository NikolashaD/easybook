-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:14:01
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.apartments
DROP TABLE IF EXISTS `apartments`;
CREATE TABLE IF NOT EXISTS `apartments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `beds` int(11) NOT NULL,
  `guests` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_id` int(10) unsigned DEFAULT NULL,
  `room_type_id` int(10) unsigned DEFAULT NULL,
  `property_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apartments_country_id_foreign` (`country_id`),
  KEY `apartments_room_type_id_foreign` (`room_type_id`),
  KEY `apartments_property_type_id_foreign` (`property_type_id`),
  CONSTRAINT `apartments_property_type_id_foreign` FOREIGN KEY (`property_type_id`) REFERENCES `property_types` (`id`) ON DELETE SET NULL,
  CONSTRAINT `apartments_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `apartments_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments: ~2 rows (approximately)
DELETE FROM `apartments`;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
INSERT INTO `apartments` (`id`, `name`, `description`, `price`, `bedrooms`, `bathrooms`, `beds`, `guests`, `created_at`, `updated_at`, `country_id`, `room_type_id`, `property_type_id`) VALUES
	(1, 'First Apartment', 'Apartment (for 2 people) \r\nA room with kitchen and balcony (TV, fridge, wireless Internet), bathroom with shower and WC.The apartment has direct access to the balcony.', 3000.00, 2, 2, 2, 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42', 2, 2, 1),
	(2, 'Second Apartment', 'Apartment (for 4+1 people)\r\n2 double bedrooms with balcony (TV, fridge, wireless Internet), 2 bathrooms with shower and WC, kitchen.\r\nThe apartment has direct access to the balcony and a view of the village and the surrounding mountains.', 2000.00, 2, 0, 2, 3, '2015-01-28 11:02:55', '2015-01-28 11:02:55', 14, 2, 1);
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
