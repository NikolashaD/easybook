<?php

class Calendar extends Eloquent {

    protected $fillable = array('status', 'date', 'from', 'to', 'information', 'note', 'apartment_id');

    public static $rules = array(
        'status' => 'required|integer',
        'apartment_id' => 'required|integer',
    );
    
    public function apartment(){
        return $this->belongsTo('Apartment');
    }
}
