<?php

class CommentsController extends BaseController{

    public function __construct(){
        //$this->beforefilter('csrf', array('on'=>'post'));
    }

    public function index()
    {
        return Response::json(Comment::get());
    }    

    public function postComment(){
        $comment = new Comment();

        $comment->content = Input::get('content');
        $comment->apartment_id = Input::get('apartment_id');
        $comment->user_id = Input::get('user_id');
        $comment->save();

        return Response::json(array('success' => true));
    }

    public function getComments($id = null){
        $apartment = Apartment::find($id);
        $currentUser = Auth::user() ? Auth::user()->id : null;
        $is_owner = false;

        $comments = array();        
        foreach($apartment->comments as $key=>$comment){
            $commentAuthor = $comment->user->id;
            if($currentUser == $commentAuthor){
                $is_owner = true;
            }

            $comments[$comment->id]['id'] = $comment->id;
            $comments[$comment->id]['ind'] = $key + 1;
            $comments[$comment->id]['authorId'] = $comment->user->id;
            $comments[$comment->id]['author'] = $comment->user->firstname.' '.$comment->user->lastname;
            $comments[$comment->id]['avatar'] = $comment->user->image;
            $comments[$comment->id]['content'] = $comment->content;       
            $comments[$comment->id]['date'] = date("F j, Y" , strtotime($comment->created_at));   
            $comments[$comment->id]['isOwner'] =  $is_owner;     
        }

        return Response::json(array_reverse($comments));
    }    


    public function destroyComment($id = null){
        Comment::destroy($id);
        return Response::json(array('success' => true));
    }    

}