<?php

class ApartmentsController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        //$this->beforefilter('admin');
        //$this->beforefilter('admin/apartments');
    }

    public function getIndex() {
        $countries = array();        
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }
        
        $roomTypes = array();        
        foreach(RoomType::all() as $roomType){
            $roomTypes[$roomType->id] = $roomType->name;
        }   
        
        $amenities = array();        
        foreach(Amenity::all() as $amenity){
            $amenities[$amenity->id] = $amenity->name;
        }   
        
        $propertyTypes = array();        
        foreach(PropertyType::all() as $propertyType){
            $propertyTypes[$propertyType->id] = $propertyType->name;
        }   
        
        $hostLanguages = array();        
        foreach(HostLanguage::all() as $hostLanguage){
            $hostLanguages[$hostLanguage->id] = $hostLanguage->name;
        }     
        
        return View::make('apartments.index')
                    ->with('apartments', Auth::user()->apartments)
                    ->with('countries', $countries)
                    ->with('roomTypes', $roomTypes)
                    ->with('amenities', Amenity::all())
                    ->with('propertyTypes', $propertyTypes)
                    ->with('hostLanguages', HostLanguage::all())
                ;
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), Apartment::$rules);

        if($validator->passes()){
            $apartment = new Apartment();
            $apartment->name = Input::get('name');
            $apartment->description = Input::get('description');   
            $apartment->price = Input::get('price');   
            $apartment->bedrooms = Input::get('bedrooms');   
            $apartment->bathrooms = Input::get('bathrooms');   
            $apartment->beds = Input::get('beds');
            $apartment->guests = Input::get('guests');  
            $apartment->city = Input::get('city');  
            $apartment->address = Input::get('address');  
            $apartment->longitude = Input::get('longitude');  
            $apartment->latitude = Input::get('latitude');  
            
            $apartment->country_id = Input::get('country_id');
            $apartment->room_type_id = Input::get('room_type_id');
            $apartment->property_type_id = Input::get('property_type_id');            
            $apartment->user_id = Input::get('user_id');

            if($apartment->save()){
                $amenities = Input::get('amenities');
                $apartment->amenities()->sync($amenities);
                
                $hostLanguages = Input::get('hostLanguages');
                $apartment->hostLanguages()->sync($hostLanguages);  
                
                $images = Input::file('images');                
                $imageNames = array();
                if(count($images) > 0 && !is_null($images[0])){                    
                    foreach($images as $key=>$image){
                        $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
                        $path = public_path('img/apartments/' . $filename);
                        Image::make($image->getRealPath())->resize(1600,620)->save($path);
                        $imageNames[$key]['image'] = 'img/apartments/'.$filename;                      
                    }                           
                    $apartment->apartmentImages()->createMany($imageNames);
                }                
            }else{
                return Redirect::back()->withErrors($apartment->errors());
            }

            return Redirect::to('admin/apartments/index')
                        ->with('message', 'New apartment has been successfully created!');
        }

        return Redirect::to('admin/apartments/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $apartment = Apartment::find(Input::get('id'));

        if($apartment){
            $apartment->name = Input::get('name');
            $apartment->description = Input::get('description');   
            $apartment->price = Input::get('price');   
            $apartment->bedrooms = Input::get('bedrooms');   
            $apartment->bathrooms = Input::get('bathrooms');   
            $apartment->beds = Input::get('beds');  
            $apartment->guests = Input::get('guests');  
            $apartment->city = Input::get('city');  
            $apartment->address = Input::get('address');  
            $apartment->longitude = Input::get('longitude');  
            $apartment->latitude = Input::get('latitude');              
            
            $apartment->country_id = Input::get('country_id');
            $apartment->room_type_id = Input::get('room_type_id');
            $apartment->property_type_id = Input::get('property_type_id');     
            $apartment->user_id = Input::get('user_id');
            
            $amenities = Input::get('amenities');
            $apartment->amenities()->sync($amenities);

            $hostLanguages = Input::get('hostLanguages');
            $apartment->hostLanguages()->sync($hostLanguages);  

            $images = Input::file('images');                
            $imageNames = array();
            if(count($images) > 0 && !is_null($images[0])){
                if($apartment->apartmentImages){
                    foreach($apartment->apartmentImages as $image){                        
                        File::delete('public/'.$image->image);    
                        $image->delete();
                    }
                }                
                
                foreach($images as $key=>$image){
                    $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
                    $path = public_path('img/apartments/' . $filename);
                    Image::make($image->getRealPath())->resize(1600,620)->save($path);
                    $imageNames[$key]['image'] = 'img/apartments/'.$filename;                      
                }                           
                $apartment->apartmentImages()->createMany($imageNames);
            }      
            $apartment->save();    
            
            return Redirect::to('admin/apartments/index')
                ->with('message', 'Apartment Updated!');
        }

        return Redirect::to('admin/apartments/index')
            ->with('message', 'Something went wrong, please, try again');
    }   

    public function postDestroy() {
        $apartment = Apartment::find(Input::get('id'));

        if($apartment){
            if($apartment->apartmentImages){
                foreach($apartment->apartmentImages as $image){
                    File::delete('public/'.$image->image);                    
                }
            }
            $apartment->delete(); 
            
            return Redirect::to('admin/apartments/index')
                ->with('message', 'Apartment Deleted!');
        }

        return Redirect::to('admin/apartments/index')
            ->with('message', 'Something went wrong, please, try again');
    }


    public function getApartments($id = null){
        $apartment = Apartment::find($id);

        $countries = array();        
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }
        
        $roomTypes = array();        
        foreach(RoomType::all() as $roomType){
            $roomTypes[$roomType->id] = $roomType->name;
        }   
        
        $propertyTypes = array();        
        foreach(PropertyType::all() as $propertyType){
            $propertyTypes[$propertyType->id] = $propertyType->name;
        }   

        return View::make('apartments.view')
                    ->with('apartment', $apartment)
                    ->with('countries', $countries)
                    ->with('roomTypes', $roomTypes)
                    ->with('amenities', Amenity::all())
                    ->with('propertyTypes', $propertyTypes)
                    ->with('hostLanguages', HostLanguage::all())                
                ;
    }

    public function getApartmentsByFilterData(){
        $countryId = Input::get('c');
        $guests = (int) Input::get('g');
        $from = Input::get('f');
        $to = Input::get('t');

        if($from){
            $fromStr = strtotime($from);
            $fromDate = date('Y-m-d', $fromStr);
        }else{            
            $fromDate = date('Y-m-d');
        }
        if($to){
            $toStr = strtotime($to);
            $toDate = date('Y-m-d', $toStr);
        }else{            
            $toDate = date('Y-m-d');
        }        
        
        if($countryId && $guests){
            $apartments = Apartment::select('apartments.*')
                                    ->where('country_id', '=', $countryId)
                                    ->where('guests', '=', $guests)
                                    ->leftJoin('calendars', 'apartments.id', '=', 'calendars.apartment_id') 
                                        ->where('calendars.from', '<=', $fromDate)    
                                        ->where('calendars.to', '>=', $toDate)      
                                        ->where('calendars.status', '=', 0) // it means 'active'                         
                                    // ->join('calendars', function($join) use ($fromDate, $toDate)
                                    // {
                                    //     $join->on('apartments.id', '=', 'calendars.apartment_id')
                                    //          ->where('calendars.from', '<=', $fromDate)
                                    //          ->where('calendars.to', '>=', $toDate)
                                    //          ->where('calendars.status', '=', 0) // it means 'active'
                                    //     ;
                                    // })
                                    ->get();
        }elseif($countryId){
            $apartments = Apartment::where('country_id', '=', $countryId)->get();
        }elseif($guests){
            $apartments = Apartment::where('guests', '=', $guests)->get();
        }else{
            $apartments = Apartment::all();
        }


        $countries = array();        
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }
        
        $roomTypes = array();        
        foreach(RoomType::all() as $roomType){
            $roomTypes[$roomType->id] = $roomType->name;
        }   
        
        $propertyTypes = array();        
        foreach(PropertyType::all() as $propertyType){
            $propertyTypes[$propertyType->id] = $propertyType->name;
        }   

        return View::make('apartments.main')
                    ->with('apartments', $apartments)
                    ->with('countries', $countries)
                    ->with('amenities', Amenity::all())
                    ->with('roomTypes', $roomTypes)
                    ->with('propertyTypes', $propertyTypes)
                    ->with('hostLanguages', HostLanguage::all()) 
        ;
    }    




    private function saveEntity($entity){

        if($entity->save()){
            return true;
        }

        return false;
    }    
}