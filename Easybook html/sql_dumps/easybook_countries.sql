-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:15:02
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.countries: ~14 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Check Republic', 'Check', 'img/countries/2015_01_28_10_47_11_check.png', '2015-01-28 10:44:36', '2015-01-28 10:47:11'),
	(2, 'Belarus', '', 'img/countries/2015_01_28_10_47_19_belarus.png', '2015-01-28 10:45:31', '2015-01-28 10:47:19'),
	(3, 'China', '', 'img/countries/2015_01_28_10_47_31_china.png', '2015-01-28 10:47:31', '2015-01-28 10:47:31'),
	(4, 'Egypt', '', 'img/countries/2015_01_28_10_47_45_egypt.png', '2015-01-28 10:47:45', '2015-01-28 10:47:45'),
	(5, 'England', '', 'img/countries/2015_01_28_10_47_54_england.png', '2015-01-28 10:47:54', '2015-01-28 10:47:54'),
	(6, 'Finland', '', 'img/countries/2015_01_28_10_48_07_finland.png', '2015-01-28 10:48:08', '2015-01-28 10:48:08'),
	(7, 'France', '', 'img/countries/2015_01_28_10_48_15_france.png', '2015-01-28 10:48:16', '2015-01-28 10:48:16'),
	(8, 'Germany', '', 'img/countries/2015_01_28_10_48_28_germany.png', '2015-01-28 10:48:28', '2015-01-28 10:48:28'),
	(9, 'Italy', '', 'img/countries/2015_01_28_10_48_43_italy.png', '2015-01-28 10:48:43', '2015-01-28 10:48:43'),
	(10, 'Japan', '', 'img/countries/2015_01_28_10_49_09_japan.png', '2015-01-28 10:49:10', '2015-01-28 10:49:10'),
	(11, 'Netherlands', '', 'img/countries/2015_01_28_10_49_23_netherlands.png', '2015-01-28 10:49:23', '2015-01-28 10:49:23'),
	(13, 'Poland', '', 'img/countries/2015_01_28_10_49_54_poland.png', '2015-01-28 10:49:54', '2015-01-28 10:49:54'),
	(14, 'Russia', '', 'img/countries/2015_01_28_10_50_14_russia.png', '2015-01-28 10:50:14', '2015-01-28 10:50:14'),
	(15, 'USA', '', 'img/countries/2015_01_28_10_50_25_usa.png', '2015-01-28 10:50:26', '2015-01-28 10:50:26');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
