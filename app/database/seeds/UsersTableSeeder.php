<?php

class UsersTableSeeder extends Seeder {
	

	public function run(){
		$user = new User();
		$user->firstname = 'Admin';
		$user->lastname = 'Adminov';
		$user->email = 'admin@gmail.com';
		$user->password = Hash::make('admin');
		$user->telephone = '911';
		$user->image = 'img/users/admin.png';
		$user->save();
	}


}