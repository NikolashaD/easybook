@extends('layouts.index')

@section('search')

<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        @if(Auth::check())
                            <li>
                               <!--  {{ HTML::image(Auth::user()->image, 'Avatar', array('class'=>'img-rounded', 'width'=>70, 'height'=>52))}} -->
                               <i style="margin-top: 16px;" class="glyphicon glyphicon-user"></i>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>{{ HTML::link('admin/dashboard', 'Dashboard')}}</li>                                    
                                    <li class="divider"></li>
                                    <li>{{ HTML::link('users/signout', 'Sign Out')}}</li>
                                </ul>
                            </li>
                        @else
                            <li>{{ HTML::link('users/signup', 'Sign Up')}}</li>
                            <li>{{ HTML::link('users/signin', 'Sign In')}}</li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Help <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>

                        <li class="for-form">
                        {{ Form::open(array('url'=>'main/apartments/', 'method'=>'get', 'style'=>'display: flex;')) }}
                            {{ Form::select('c', $filterCountries,'', array('class'=>'form-control')) }}
                            {{ Form::text('f', '', array('class'=>'form-control tcal tcalInput', 'placeholder'=>'Check In')) }}
                            {{ Form::text('t', '', array('class'=>'form-control tcal tcalInput', 'placeholder'=>'Check Out')) }}
                            {{ Form::select('g', array('Guests','1','2','3','4','5','6','7','8','9','10'),'', array('class'=>'form-control')) }}
                            {{ Form::submit('Search', array('class'=>'btn btn-success', 'style'=>'padding-right:45px;')) }}
                        {{ Form::close() }}
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>

@stop


@section('content')

<h1 style="padding-left:42%;">Explore the World</h1>
<div style="background-color: white;" class="container marketing">
    <hr>

    <div class="row featurette">
        @foreach($countries as $key=>$country)
        <a href="{{ URL::to('/main/apartments/?c='.$country->id) }}">
            <div class="col-md-5">
                {{ HTML::image($country->image, $country->name, array('class'=>'featurette-image img-responsive')) }}
                <span class="img-responsive">{{ $country->name }}</span>
            </div>

        </a>
        @endforeach
    </div>


@stop