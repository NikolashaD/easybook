@extends('layouts.main')

@section('content')
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize() {
        var point = new google.maps.LatLng(53.904540, 27.561524);

              var mapProp = {
                center: point,
                zoom:10,
                mapTypeId:google.maps.MapTypeId.ROADMAP
              };
              var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var marker = new google.maps.Marker({
            position: point,
            map: map,
            title:"Hello World!"
        });      
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <h2>Map</h2>
          <p></p>
	        <div class="map">
	            <div id="googleMap" style="width:100%;height:1000px;"></div>
	        </div>          
        </div>
        <div class="col-md-5">
			<div class="">
				<h2>Filter</h2>
				<p></p>
				<p></p>
            <table class="table">
            <thead>
                <tr class="grey">
                    <td width="250px"><h4>{{ Form::label('Info') }}</h4></td>
                    <td>{{ Form::label('From') }} {{ Form::text('from', '', array('class'=>'form-control tcal tcalInput')) }}</td>                    
                    <td>{{ Form::label('To') }}{{ Form::text('to', '', array('class'=>'form-control tcal tcalInput')) }}</td>    
                    <td>{{ Form::label('Guests') }}{{ Form::select('guests', array('1','2','3','4','5','6','7','8','9','10'),'', array('class'=>'form-control')) }}</td>                                      
                </tr>
            </thead>            
            <tbody class="hide">
                <tr>
                    <td><h4>{{ Form::label('Location') }}</h4></td>
                    <td>{{ Form::label('Country') }} {{ Form::select('country_id', $countries,'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('City') }} {{ Form::text('city', '', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Low Price') }} {{ Form::text('price', '', array('class'=>'form-control')) }}</td>
                </tr>   
                <tr>
                    <td></td>                    
                    <td>{{ Form::label('Room type') }} {{ Form::select('room_type_id', $roomTypes,'', array('class'=>'form-control')) }}</td>                    
                    <td>{{ Form::label('Property type') }} {{ Form::select('property_type_id', $propertyTypes,'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('High Price') }} {{ Form::text('price', '', array('class'=>'form-control')) }}</td>
                </tr>  
                <tr>
                    <td><h4>{{ Form::label('Size') }}</h4></td>
                    <td>{{ Form::label('Bedrooms') }} {{ Form::select('bedrooms', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Bathrooms') }} {{ Form::select('bathrooms', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Beds') }} {{ Form::select('beds', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                </tr>                   
                
                @for ($i = 0; $i < count($amenities); $i+=3)                   
                <tr @if($i == 0) class="grey" @endif>
                    
                    <td>
                        @if($i == 0)
                            <h4>{{ Form::label('Amenities') }}</h4>
                        @endif
                    </td>
                    <td>  
                        @if(isset($amenities[$i]))
                            {{ Form::checkbox('amenities[]', $amenities[$i]->id); }}
                            {{ $amenities[$i]->name }} 
                        @endif
                    </td>                        
                    <td>     
                        @if(isset($amenities[$i+1]))
                            {{ Form::checkbox('amenities[]', $amenities[$i+1]->id); }}
                            {{ $amenities[$i+1]->name }} 
                        @endif
                    </td>
                    <td>    
                        @if(isset($amenities[$i+2]))
                            {{ Form::checkbox('amenities[]', $amenities[$i+2]->id); }}
                            {{ $amenities[$i+2]->name }} 
                        @endif
                    </td> 
                </tr>   
                @endfor   
                
                @for ($i = 0; $i < count($hostLanguages); $i+=3)                 
                <tr @if($i == 0) class="grey" @endif>
                    <td>
                        @if($i == 0)
                            <h4>{{ Form::label('Host languages') }}</h4>
                        @endif
                    </td>
                    <td>  
                        @if(isset($hostLanguages[$i]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i]->id); }}
                            {{ $hostLanguages[$i]->name }} 
                        @endif
                    </td>                        
                    <td>     
                        @if(isset($hostLanguages[$i+1]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+1]->id); }}
                            {{ $hostLanguages[$i+1]->name }} 
                        @endif
                    </td>
                    <td>    
                        @if(isset($hostLanguages[$i+2]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+2]->id); }}
                            {{ $hostLanguages[$i+2]->name }} 
                        @endif
                    </td> 
                </tr>   
                @endfor
            </tbody>
                
            </table>
			</div>
			
			<hr />
			
			<div>
				<h2>Apartments</h2>
				@foreach($apartments as $ind=>$apartment)
					<div class="block">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->						  
						  <ol class="carousel-indicators">
						  	@foreach($apartment->apartmentImages as $key=>$image)
						  		@if($key == 0)
						  			<li data-target="#carousel-example-generic" data-slide-to="{{ $key }}" class="active"></li>
						  		@else
									<li data-target="#carousel-example-generic" data-slide-to="{{ $key }}"></li>
								@endif
							@endforeach
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						  	@foreach($apartment->apartmentImages as $key=>$image)
						  		@if($key == 0)
									<div class="item active">
									  {{ HTML::image($image->image, '', array('class'=>'slider')) }}
									</div>
								@else
									<div class="item">
									  {{ HTML::image($image->image, '', array('class'=>'slider')) }}
									</div>									
								@endif
							@endforeach
						  </div>
						  <!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>						
						</div>


						<div class="price small-block3">
							<span class="big-white">$<b>{{ $apartment->price }}</b></span> <span class="small-grey">per day</span> 
						</div>

						<div class="small-block1">
							{{ HTML::image($apartment->user->image, '', array('class'=>'img-thumbnail avatar')) }}
						</div>

						<div class="small-block2">
						{{ HTML::link('main/apartments/'.$apartment->id , $apartment->name, array('class'=>'title')) }}
							<p class="small-grey">{{ Str::limit($apartment->description, 100) }}</p>
						</div>
					</div>
				@endforeach				
			</div>		  
       </div>
      </div>
    </div>

@stop