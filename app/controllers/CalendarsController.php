<?php

class CalendarsController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/calendars');
    }

    public function getIndex() {
        $apartmentsNames = array();        
        foreach(Auth::user()->apartments as $apartment){
            $apartmentsNames[$apartment->id] = $apartment->name;
        }
        return View::make('apartments.calendar')
        		->with('apartmentsNames', $apartmentsNames)
        		->with('apartments', Auth::user()->apartments);
    }   

    public function postCreate() {
        $validator = Validator::make(Input::all(), Calendar::$rules);

        if($validator->passes()){
            $calendar = new Calendar();
            $this->saveCalendar($calendar);

            return Redirect::to('admin/calendars/index')
                        ->with('message', 'New reservation has been successfully created!');
            
        }

        return Redirect::to('admin/calendars/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $calendar = Calendar::find(Input::get('id'));

        if($calendar){
            $this->saveCalendar($calendar);
            
            return Redirect::to('admin/calendars/index')
                ->with('message', 'Reservation Updated!');
        }

        return Redirect::to('admin/calendars/index')
            ->with('message', 'Something went wrong, please, try again');
    }   

    public function postDestroy() {
        $calendar = Calendar::find(Input::get('id'));

        if($calendar){
            $calendar->delete(); 
            
            return Redirect::to('admin/calendars/index')
                ->with('message', 'Reservation Deleted!');
        }

        return Redirect::to('admin/calendars/index')
            ->with('message', 'Something went wrong, please, try again');
    }



    private function saveCalendar($calendar){
        $value = Input::get('date');
        if(!empty($value)){
            $date = strtotime(Input::get('date'));
            $newDateFormat = date('Y-m-d h:i:s',$date);
        }else{
            $newDateFormat = null;
        }                
        $calendar->date = $newDateFormat;

        $value = Input::get('from');
        if(!empty($value)){
            $from = strtotime(Input::get('from'));
            $newFromFormat = date('Y-m-d h:i:s',$from);
        }else{
            $newFromFormat = null;
        }
        $calendar->from = $newFromFormat;

        $value = Input::get('to');
        if(!empty($value)){
            $to = strtotime(Input::get('to'));
            $newToFormat = date('Y-m-d h:i:s',$to);   
        }else{
            $newToFormat = null;
        }         
        $calendar->to = $newToFormat;

        $calendar->information = Input::get('information');
        $calendar->note = Input::get('note');
        $calendar->status = Input::get('status');
        
        $calendar->apartment_id = Input::get('apartment_id');
        
        if($calendar->save()){
            return true;
        }

        return false;
    }    

}