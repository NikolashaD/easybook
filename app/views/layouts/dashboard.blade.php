<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EB Admin Panel</title>
    <link rel="icon" href={{ asset('img/favicon.png') }}>

    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/jquery-ui-1.10.4.custom.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/plugins/raphael.min.js') }}
    {{ HTML::script('js/plugins/morris.min.js') }}
    {{ HTML::script('js/plugins/morris-data.js') }}
    {{ HTML::script('js/dropzone.js') }}    
    {{ HTML::script('js/index.js') }}
    {{ HTML::script('js/plugins/tcal.js') }}

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/sb-admin.css') }}
    {{ HTML::style('css/plugins/morris.css') }}
    {{ HTML::style('css/basic.css');}}
    {{ HTML::style('css/dropzone.css') }}
    {{ HTML::style('css/ui-lightness/jquery-ui-1.10.4.custom.css') }}    
    {{ HTML::style('font-awesome/css/font-awesome.min.css') }}
    {{ HTML::style('css/plugins/tcal.css') }}

</head>

<body>

<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">        
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a style="padding-bottom:0!important" href="{{ URL::to('admin/dashboard/') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ HTML::image('img/logo.png', 'EasyBook', array('class'=>'dashboard-logo')) }}<span style="font-size:20px;">&nbsp;&nbsp;Admin Panel</span>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu" style="left:50px;">
                    <li>{{ HTML::link('/', 'Visit Site')}}</li>                                    
                    <li class="divider"></li>
                    <li>{{ HTML::link('users/signout', 'Sign Out')}}</li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">

                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">

                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-preview">
                    <a href="#">
                        <div class="media">
                                    <span class="pull-left">

                                    </span>
                            <div class="media-body">
                                <h5 class="media-heading"><strong>John Smith</strong>
                                </h5>
                                <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                <p>Lorem ipsum dolor sit amet, consectetur...</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="message-footer">
                    <a href="#">Read All New Messages</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                </li>
                <li>
                    <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{Auth::user()->firstname}} {{Auth::user()->lastname}} <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ URL::to('users/profile') }}"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ URL::to('users/signout') }}"><i class="fa fa-fw fa-power-off"></i> Sign Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li @if(Request::is('admin/dashboard') ) class="active" @endif>
                <a href="{{ URL::to('admin/dashboard/') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>

            @if(Auth::user()->role->name == 'SUPER_ADMIN')
                <li @if(Request::is('admin/countries') ) class="active" @endif>
                    <a href="{{ URL::to('admin/countries/') }}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Countries</a>                  
                </li>
                <li @if(Request::is('admin/roomTypes') ) class="active" @endif>
                    <a href="{{ URL::to('admin/roomTypes/') }}"><i class="fa fa-fw fa-edit"></i>Room Types</a>
                </li>             
                <li @if(Request::is('admin/amenities') ) class="active" @endif>
                    <a href="{{ URL::to('admin/amenities/') }}"><i class="fa fa-fw fa-edit"></i>Amenities</a>
                </li>  
                <li @if(Request::is('admin/propertyTypes') ) class="active" @endif>
                    <a href="{{ URL::to('admin/propertyTypes/') }}"><i class="fa fa-fw fa-edit"></i>Property Types</a>
                </li>   
                <li @if(Request::is('admin/hostLanguages') ) class="active" @endif>
                    <a href="{{ URL::to('admin/hostLanguages/') }}"><i class="glyphicon glyphicon-book"></i> Host Languages</a>
                </li>             
            @endif

            @if(Auth::user()->role->name == 'LANDLORD')
                <li @if(Request::is('admin/apartments') ) class="active" @endif>
                    <a href="{{ URL::to('admin/apartments/') }}"><i class="glyphicon glyphicon-home"></i> Apartments</a>
                </li>      
                <li @if(Request::is('admin/calendars') ) class="active" @endif>
                    <a href="{{ URL::to('admin/calendars/') }}"><i class="glyphicon glyphicon-calendar"></i> Calendar</a>
                </li>  
            @endif            

            @if(Auth::user()->role->name == 'LANDLORD' || Auth::user()->role->name == 'TENANT')
                <li @if(Request::is('admin/wishlist') ) class="active" @endif>
                    <a href="{{ URL::to('admin/wishlist/') }}"><i class="glyphicon glyphicon-heart-empty"></i> Wish List</a>
                </li>    
                <li @if(Request::is('admin/requests') ) class="active" @endif>
                    <a href="{{ URL::to('admin/requests/') }}"><i class="glyphicon glyphicon-credit-card"></i> Requests</a>
                </li>                  
            @endif               

            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                    <li>
                        <a href="#">Dropdown Item</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">

<div class="container-fluid">
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif

    @yield('content')
</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



</body>

</html>
