<?php

class Rating extends Eloquent {

    protected $fillable = array('points', 'apartment_id', 'user_id');

    public static $rules = array(
        'points' => 'required',       
        'apartment_id' => 'required|integer',
        'user_id' => 'required|integer',
    );
    
    public function apartment(){
        return $this->belongsTo('Apartment');
    }  

    public function user(){
        return $this->belongsTo('User');
    }    
}