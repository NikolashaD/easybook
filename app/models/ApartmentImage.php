<?php

class ApartmentImage extends Eloquent {

    protected $fillable = array('image');

    public static $rules = array(
        'image' => 'image|mimes:jpg,jpeg,png,bmp,gif'
    );

    
    public function apartment(){
        return $this->belongsTo('Apartment', 'apartments');
    }   
   
}
