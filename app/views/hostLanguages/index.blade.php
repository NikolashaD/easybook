@extends('layouts.dashboard')

@section('content')
<div id="admin">
    <h1>Host Languages Admin Panel</h1>

    <p>Here you can view, delete, and create new amenities.</p>


    <hr />
    @if($errors->has())
    <div >
        <p class="label label-danger"> The following errors have occurred: </p>
        <ul>
            @foreach($errors->all() as $error)
            <li class="red">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ Form::open(array('url'=>'admin/hostLanguages/create')) }}
    <div class="form-inline">

        <div class="form-group">
            {{ Form::text('name', '', array('class'=>'form-control')) }}
        </div>
        {{ Form::submit('Create', array('class'=>'btn btn-success')) }}

    </div>
    {{ Form::close() }}

    <h2>Host Languages</h2>
    <hr />

    <table class="table table-striped">
        <thead>
            <tr>
                <th width="10%">№</th>
                <th width="70%">Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($hostLanguages as $key=>$hostLanguage)
            <tr class="show-grey-simple">
                <td>
                    {{ $key + 1 }}
                </td>
                <td>
                    {{ $hostLanguage->name }}
                </td>
                <td>
                    {{ Form::open(array('url'=>'admin/hostLanguages/destroy', 'class'=>'form-inline')) }}
                    {{ Form::hidden('id', $hostLanguage->id) }}
                    {{ Form::submit('delete', array('class'=>'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
            </tr>
            <tr class="hide show-white">
                <td></td>
                <td>
                    {{ Form::open(array('url'=>'admin/hostLanguages/update', 'class'=>'form-inline')) }}
                    {{ Form::text('name', $hostLanguage->name, array('class'=>'form-control')) }}
                </td>
                <td>                        
                    {{ Form::hidden('id', $hostLanguage->id) }}
                    {{ Form::submit('save', array('class'=>'btn btn-success')) }}
                    {{ Form::close() }}
                </td>
            </tr>             
            @endforeach
        </tbody>
    </table>

    {{ Form::open(array('url'=>'admin/hostLanguages/create')) }}
    <div class="form-inline">

        <div class="form-group">
            {{ Form::text('name', '', array('class'=>'form-control')) }}
        </div>
        {{ Form::submit('Create', array('class'=>'btn btn-success')) }}

    </div>
    {{ Form::close() }}

</div>
<br />

@stop