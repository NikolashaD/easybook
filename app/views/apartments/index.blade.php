@extends('layouts.dashboard')

@section('content')
<div id="admin">
    <h1>Apartments Admin Panel</h1>

    <p>Here you can view, delete, and create new apartments.</p>


    <hr />
    @if($errors->has())
    <div >
        <p class="label label-danger"> The following errors have occurred: </p>
        <ul>
            @foreach($errors->all() as $error)
            <li class="red">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {{ Form::open(array('url'=>'admin/apartments/create', 'files'=>true)) }}
    <div class="form-inline">

        <div class="form-group" style="width:100%;">
            <table class="table table-hover">
            <thead>
                <tr class="grey">
                    <td width="250px"><h4>{{ Form::label('Info') }}</h4></td>
                    <td  width="429px" colspan="2">
                        {{ Form::label('Title') }} {{ Form::text('name', '', array('class'=>'form-control')) }}
                    </td>                    
                    <td>{{ Form::label('Price') }}{{ Form::text('price', '', array('class'=>'form-control')) }}</td>                                      
                </tr>
            </thead>
            <tbody class="hide">
                <tr class="grey">
                    <td><h4>{{ Form::label('Description') }}</h4></td>
                    <td colspan="3">{{ Form::textarea('description', '', array('class'=>'form-control')) }}</td>
                </tr>
                <tr class="grey">
                    <td><h4>{{ Form::label('Location') }}</h4></td>
                    <td>{{ Form::label('Country') }} {{ Form::select('country_id', $countries,'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Room type') }} {{ Form::select('room_type_id', $roomTypes,'', array('class'=>'form-control')) }}</td>                    
                    <td>{{ Form::label('Property type') }} {{ Form::select('property_type_id', $propertyTypes,'', array('class'=>'form-control')) }}</td>
                </tr>   
                <tr class="grey">
                    <td></td>
                    <td>{{ Form::label('Address') }} {{ Form::text('address', '', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('City') }} {{ Form::text('city', '', array('class'=>'form-control')) }}</td>                                        
                    <td>
                        {{ Form::label('Latitude', '',  array('style'=>'padding-left:100px; padding-right:100px;')) }} 
                        {{ Form::label('Longitude', '', array('style'=>'padding-left:100px;')) }}  
                        {{ Form::text('latitude', '', array('class'=>'form-control', 'style'=>'width:48%!important')) }}
                        {{ Form::text('longitude', '', array('class'=>'form-control', 'style'=>'width:48%!important' )) }}
                    </td>
                </tr>  
                <tr class="grey">
                    <td><h4>{{ Form::label('Size') }}</h4></td>
                    <td>{{ Form::label('Bedrooms') }} {{ Form::select('bedrooms', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Bathrooms') }} {{ Form::select('bathrooms', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Beds') }} {{ Form::select('beds', array('1','2','3','4','5'),'', array('class'=>'form-control')) }}</td>
                </tr>                   
                
                @for ($i = 0; $i < count($amenities); $i+=3)                   
                <tr @if($i == 0) class="grey" @endif>
                    
                    <td>
                        @if($i == 0)
                            <h4>{{ Form::label('Amenities') }}</h4>
                        @endif
                    </td>
                    <td>  
                        @if(isset($amenities[$i]))
                            {{ Form::checkbox('amenities[]', $amenities[$i]->id); }}
                            {{ $amenities[$i]->name }} 
                        @endif
                    </td>                        
                    <td>     
                        @if(isset($amenities[$i+1]))
                            {{ Form::checkbox('amenities[]', $amenities[$i+1]->id); }}
                            {{ $amenities[$i+1]->name }} 
                        @endif
                    </td>
                    <td>    
                        @if(isset($amenities[$i+2]))
                            {{ Form::checkbox('amenities[]', $amenities[$i+2]->id); }}
                            {{ $amenities[$i+2]->name }} 
                        @endif
                    </td> 
                </tr>   
                @endfor   
                
                @for ($i = 0; $i < count($hostLanguages); $i+=3)                 
                <tr @if($i == 0) class="grey" @endif>
                    <td>
                        @if($i == 0)
                            <h4>{{ Form::label('Host languages') }}</h4>
                        @endif
                    </td>
                    <td>  
                        @if(isset($hostLanguages[$i]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i]->id); }}
                            {{ $hostLanguages[$i]->name }} 
                        @endif
                    </td>                        
                    <td>     
                        @if(isset($hostLanguages[$i+1]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+1]->id); }}
                            {{ $hostLanguages[$i+1]->name }} 
                        @endif
                    </td>
                    <td>    
                        @if(isset($hostLanguages[$i+2]))
                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+2]->id); }}
                            {{ $hostLanguages[$i+2]->name }} 
                        @endif
                    </td> 
                </tr>   
                @endfor
                <tr class="grey">
                    <td><h4>{{ Form::label('Images', 'Choose images') }}</h4></td>
                    <td>{{ Form::file('images[]', array('multiple'=>true)) }}</td>
                    <td>{{ Form::label('Guests') }} </td>
                    <td>{{ Form::select('guests', array('1','2','3','4','5','6','7','8','9','10'),'', array('class'=>'form-control')) }}</td>
                </tr> 
            </tbody>
                
            </table>
        </div>
        <br />
        {{ Form::hidden('user_id', Auth::user()->id) }}
        <div style="float:left; width:50%;">{{ Form::submit('Create', array('class'=>'btn btn-success')) }}</div>
        <div style="float:right;">{{ Form::submit('Create', array('class'=>'btn btn-success')) }}</div>

    </div>    
    {{ Form::close() }}
    
    <br /><br />
    <h2>Apartments</h2>
    <hr />

    <table class="table">
        <thead>
            <tr>
                <th width="5%">№</th>
                <th width="10%">Image</th>
                <th width="25%">Title</th>
                <th width="25%">Country</th>
                <th width="25%">Price $</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($apartments as $key=>$apartment)
            <div class="hide"> 
                @foreach($apartment->amenities as $ka=>$a)
                    {{ $apartmentAmenities[$ka] = $a->id; }}
                @endforeach
                <br />
                @foreach($apartment->hostLanguages as $kh=>$h)
                    {{ $apartmentHostlanguages[$kh] = $h->id; }}
                @endforeach  
                <br /><br />
            </div>
            <tr class="show-grey">
                <td>
                    {{ $key + 1 }}
                </td>
                <td>
                    @if(isset($apartment->apartmentImages[0]))
                        {{ HTML::image($apartment->apartmentImages[0]->image, $apartment->name, array('width'=>'100')) }}
                    @endif
                </td>                
                <td>
                    {{ $apartment->name }}
                </td>
                <td>
                    @if($apartment->country)                    
                        {{ $apartment->country->name }}
                    @endif                         
                </td>            
                <td>                 
                    {{ $apartment->price }} $                  
                </td>                
                <td>                    
                    {{ Form::open(array('url'=>'admin/apartments/destroy', 'class'=>'form-inline')) }}
                    {{ Form::hidden('id', $apartment->id) }}
                    {{ HTML::link('main/apartments/'.$apartment->id.'','', array('class'=>'glyphicon glyphicon-search btn btn-info'))}}
                    {{ Form::submit('delete', array('class'=>'btn btn-danger')) }}                                        
                    {{ Form::close() }}
                </td>
            </tr>

                {{ Form::open(array('url'=>'admin/apartments/update', 'files'=>true)) }}
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Info') }}</td>                  
                    <td colspan="2">{{ Form::label('Title') }} {{ Form::text('name', $apartment->name, array('class'=>'form-control')) }}</td> 
                    <td>{{ Form::label('Price') }} {{ Form::text('price', $apartment->price, array('class'=>'form-control')) }}</td>    
                    <td></td>                 
                </tr>             
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Description') }}</td>                  
                    <td colspan="3">{{ Form::textarea('description', $apartment->description, array('class'=>'form-control')) }}</td> 
                    <td></td>                 
                </tr>       
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Location') }}</td>                  
                    <td>{{ Form::label('Country') }}  @if($apartment->country)  {{ Form::select('country_id', $countries, $apartment->country->id, array('class'=>'form-control')) }} @else {{ Form::select('country_id', $countries, '', array('class'=>'form-control')) }}  @endif</td>
                    <td>{{ Form::label('Room type') }} {{ Form::select('room_type_id', $roomTypes, $apartment->roomType->id, array('class'=>'form-control')) }}</td>                    
                    <td>{{ Form::label('Property type') }} {{ Form::select('property_type_id', $propertyTypes, $apartment->propertyType->id, array('class'=>'form-control')) }}</td>
                    <td></td>                 
                </tr>  
                <tr class="hide">
                    <td></td>                
                    <td></td>   
                    <td>{{ Form::label('City') }} {{ Form::text('city', $apartment->city, array('class'=>'form-control')) }}</td>       
                    <td>{{ Form::label('Address') }} {{ Form::text('address', $apartment->address, array('class'=>'form-control')) }}</td>                    
                    <td>
                        {{ Form::label('Latitude', '',  array('style'=>'padding-left:40px; padding-right:50px;')) }} 
                        {{ Form::label('Longitude', '', array('style'=>'padding-left:30px;')) }}  
                        {{ Form::text('latitude', $apartment->latitude, array('class'=>'form-control', 'style'=>'width:50%!important; float:left')) }}
                        {{ Form::text('longitude', $apartment->longitude, array('class'=>'form-control', 'style'=>'width:50%!important' )) }}                 
                    </td>
                    <td></td>                 
                </tr>                 
                <tr class="hide">
                    <td></td>                
                    <td>{{ Form::label('Size') }}</td>                  
                    <td>{{ Form::label('Bedrooms') }} {{ Form::select('bedrooms', array('1','2','3','4','5'), $apartment->bedrooms, array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Bathrooms') }} {{ Form::select('bathrooms', array('1','2','3','4','5'), $apartment->bathrooms, array('class'=>'form-control')) }}</td>
                    <td>{{ Form::label('Beds') }} {{ Form::select('beds', array('1','2','3','4','5'), $apartment->beds, array('class'=>'form-control')) }}</td>
                    <td></td>                 
                </tr> 
                    @for ($i = 0; $i < count($amenities); $i+=3)     
                    <tr class="hide">
                        <td></td>
                        <td>
                            @if($i == 0)
                                {{ Form::label('Amenities') }}
                            @endif
                        </td>
                        <td>  
                            @if(isset($amenities[$i]))
                                {{ Form::checkbox('amenities[]', $amenities[$i]->id, in_array($amenities[$i]->id, $apartmentAmenities)); }}
                                {{ $amenities[$i]->name }} 
                            @endif
                        </td>                        
                        <td>     
                            @if(isset($amenities[$i+1]))
                                {{ Form::checkbox('amenities[]', $amenities[$i+1]->id, in_array($amenities[$i+1]->id, $apartmentAmenities)); }}
                                {{ $amenities[$i+1]->name }} 
                            @endif
                        </td>
                        <td>    
                            @if(isset($amenities[$i+2]))
                                {{ Form::checkbox('amenities[]', $amenities[$i+2]->id, in_array($amenities[$i+2]->id, $apartmentAmenities)); }}
                                {{ $amenities[$i+2]->name }} 
                            @endif
                        </td> 
                        <td></td>  
                    </tr>   
                    @endfor   

                    @for ($i = 0; $i < count($hostLanguages); $i+=3)                 
                    <tr class="hide">
                        <td></td> 
                        <td>
                            @if($i == 0)
                                {{ Form::label('Host languages') }}
                            @endif
                        </td>
                        <td>  
                            @if(isset($hostLanguages[$i]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i]->id, in_array($hostLanguages[$i]->id, $apartmentHostlanguages)); }}
                                {{ $hostLanguages[$i]->name }} 
                            @endif
                        </td>                        
                        <td>     
                            @if(isset($hostLanguages[$i+1]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+1]->id, in_array($hostLanguages[$i+1]->id, $apartmentHostlanguages)); }}
                                {{ $hostLanguages[$i+1]->name }} 
                            @endif
                        </td>
                        <td>    
                            @if(isset($hostLanguages[$i+2]))
                                {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+2]->id, in_array($hostLanguages[$i+2]->id, $apartmentHostlanguages)); }}
                                {{ $hostLanguages[$i+2]->name }} 
                            @endif
                        </td>
                        <td></td> 
                    </tr>   
                    @endfor        
                    <tr class="hide">
                        <td></td>
                        <td>{{ Form::label('Image', 'Image') }}</td>
                        <td>
                            @foreach($apartment->apartmentImages as $image)
                            <div style="float:left; padding:5px;">{{ HTML::image($image->image, '', array('width'=>'120',)) }}</div>
                            @endforeach
                        </td>
                        <td>{{ Form::label('Image', 'Choose new images') }}{{ Form::file('images[]', array('multiple'=>true)) }}</td>
                        <td>{{ Form::label('Guests') }} {{ Form::select('guests', array('1','2','3','4','5','6','7','8','9','10'),$apartment->guests, array('class'=>'form-control')) }}</td>
                        <td>                    
                            {{ Form::hidden('id', $apartment->id) }}
                            {{ Form::hidden('user_id', Auth::user()->id) }}
                            {{ Form::submit('save', array('class'=>'btn btn-success', 'style'=>'margin-top: 25px;')) }}
                            {{ Form::close() }}
                        </td>
                    </tr> 
                    <tr class="hide">
                        <td colspan="6"></td>
                    </tr>                  
                {{ Form::close() }}

            @endforeach            
        </tbody>
    </table>
</div>
<br />
@stop