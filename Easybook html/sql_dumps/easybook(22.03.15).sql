-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.17 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-03-22 21:33:29
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
-- Dumping data for table easybook.amenities: ~27 rows (approximately)
DELETE FROM `amenities`;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Wireless Internet', '2014-12-29 15:11:23', '2014-12-29 15:11:23'),
	(2, 'TV', '2014-12-29 15:11:47', '2014-12-29 15:11:47'),
	(3, 'Kitchen', '2014-12-29 15:11:55', '2014-12-29 15:11:55'),
	(4, 'Air Conditioning', '2014-12-29 15:12:04', '2014-12-29 15:12:04'),
	(5, 'Breakfast', '2014-12-29 15:12:08', '2014-12-29 15:12:08'),
	(6, 'Cable TV', '2014-12-29 15:12:11', '2014-12-29 15:12:11'),
	(7, 'Doorman', '2014-12-29 15:12:15', '2014-12-29 15:12:15'),
	(8, 'Dryer', '2014-12-29 15:12:19', '2014-12-29 15:12:19'),
	(9, 'Elevator in Building', '2014-12-29 15:12:23', '2014-12-29 15:12:23'),
	(10, 'Essentials', '2014-12-29 15:12:27', '2014-12-29 15:12:27'),
	(11, 'Family/Kid Friendly', '2014-12-29 15:12:31', '2014-12-29 15:12:31'),
	(12, 'Fire Extinguisher', '2014-12-29 15:12:34', '2014-12-29 15:12:34'),
	(13, 'First Aid Kit', '2014-12-29 15:12:38', '2014-12-29 15:12:38'),
	(14, 'Gym', '2014-12-29 15:12:43', '2014-12-29 15:12:43'),
	(15, 'Heating', '2014-12-29 15:12:46', '2014-12-29 15:12:46'),
	(16, 'Hot Tub', '2014-12-29 15:12:49', '2014-12-29 15:12:49'),
	(17, 'Indoor Fireplace', '2014-12-29 15:12:52', '2014-12-29 15:12:52'),
	(18, 'Internet', '2014-12-29 15:12:57', '2014-12-29 15:12:57'),
	(19, 'Pets Allowed', '2014-12-29 15:13:02', '2014-12-29 15:13:02'),
	(20, 'Pool', '2014-12-29 15:13:05', '2014-12-29 15:13:05'),
	(21, 'Safety Card', '2014-12-29 15:13:08', '2014-12-29 15:13:08'),
	(22, 'Shampoo', '2014-12-29 15:13:12', '2014-12-29 15:13:12'),
	(23, 'Smoke Detector', '2014-12-29 15:13:16', '2014-12-29 15:13:16'),
	(24, 'Smoking Allowed', '2014-12-29 15:13:19', '2014-12-29 15:13:19'),
	(25, 'Suitable for Events', '2014-12-29 15:13:23', '2014-12-29 15:13:23'),
	(26, 'Washer', '2014-12-29 15:13:27', '2014-12-29 15:13:27'),
	(27, 'Pen', '2015-02-15 16:01:08', '2015-02-15 16:01:20');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;

-- Dumping data for table easybook.apartments: ~7 rows (approximately)
DELETE FROM `apartments`;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
INSERT INTO `apartments` (`id`, `name`, `description`, `price`, `bedrooms`, `bathrooms`, `beds`, `guests`, `city`, `address`, `longitude`, `latitude`, `created_at`, `updated_at`, `country_id`, `room_type_id`, `property_type_id`, `user_id`) VALUES
	(2, 'Second Apartment', 'Apartment (for 2 people) \r\nA room with kitchen and balcony (TV, fridge, wireless Internet), bathroom with shower and WC.The apartment has direct access to the balcony.', 80.00, 0, 0, 0, 5, 'London', 'London av. 32-6-32', '-0.127758', '51.507351', '2015-02-07 19:30:11', '2015-02-12 19:36:08', 5, 2, 1, 4),
	(3, 'Third Apartment', 'The apartments at Golden Sand Hotel have been designed in a way that will offer you lots of space (quite spacious, about 45sq.m) and with many amenities set to comfort your stay, but most of all, privacy – for those who treasure their quiet time during their holiday.\r\nThe apartments are bright, airy, and beautifully decorated.\r\nAll rooms overlook the swimming pool area and gardens, or have quick access to them.\r\nThe apartments are ideal for couples as well as a small family.\r\nEach apartment is fully equipped, with kitchen and free internet access.', 30.00, 2, 2, 2, 0, 'Vaasa', 'Yolopuki pr. 32-3-5', '21.616513', '63.095141', '2015-02-10 17:17:57', '2015-02-12 18:05:42', 6, 3, 2, 4),
	(4, 'Full Apartment', 'In a sought-after building on the seafront, an apartment that is currently used as office space in good condition, with a living space of approximately 160m2 on 2 levels, the upper floor with a sea view, ideal for professional. Real opportunity.', 70.00, 2, 3, 4, 3, 'Berlin', 'Partizanski av. 32-32-61', '13.404954', '52.520007', '2015-02-12 17:52:45', '2015-02-12 18:35:40', 8, 3, 3, 4),
	(5, 'So beautiful apartment', 'Apartment occupancy varies from six to ten residents. All rooms are single-occupancy rooms, while size varies considerably. The size of the rooms ranges from 9 to 18,5 square meters. All rooms are furnished with writing desk, chair, bookshelves, bed, mattress and curtains. Students supply any linens, rugs and decorations needed.\r\nEach occupant may freely use the apartments communal area: livingroom, bathroom and kitchen. Apartments have facilities which include some comfortable seating in the living room, a dining table and chairs in the kitchen or dining room; a stove, a microwave oven, two refridgerators and a freezer in the kitchen and a drying cupboard. Students provide their own dishes and cooking utensils. Occupants usually have some kind of arrangement on the communal use of pots and pans, coffeemakers etc.\r\nLaundry machines and sauna are available in the village area. Dining facilities are located nearby in the University buildings. Allergy-causings pets, such as cats and dogs are not allowed in the apartments, neither smoking inside the buildings.', 25.00, 1, 1, 1, 2, 'Brno', 'Jakubska 2', '16.606808', '49.196045', '2015-02-16 13:06:20', '2015-02-16 13:06:20', 1, 2, 4, 6),
	(6, 'La casa tra li ulivi', 'L\'alloggio è speciale con vista mozzafiato, una sauna due posti per rilassarsi e godere della tranquillità; possiamo tirare con l\'arco, fare l\'orto, zappare le patate, conoscere le api ,oppure se lo desiderate visitare i tanti stupendi luoghi nei dintorni..\r\n\r\nAccesso a tutto quello che desiderano!\r\n\r\nInterazione al cento per cento.. se lo desiderano possiamo fare anche la pizza al forno a legna, poi il resto viene da solo! gite turistiche con volvo cabriolet(a pagamento)\r\n\r\nLa calma, tranqullità.. molti luoghi da visitare nei dintorni, buona cucina e tanto altro..\r\n\r\nDiamo la nostra disponibilità ,per visitare luoghi e persone interessanti,e di loro interesse\r\n\r\nSarei felice di ospitare anche artisti. \r\nParlo correttamente il francese, ma per le occasioni anche inglese.', 55.00, 2, 2, 3, 3, 'Bellegra', 'Vitellia', '13.029065', '41.879651', '2015-02-16 13:15:49', '2015-02-16 13:15:49', 9, 1, 1, 7),
	(7, 'Apartment in Central Berlin', 'Charming 1-bedroom appartment in central Berlin. \r\nBuilding from 1890, very quiet even though it is centrally located on the centre, high ceilings, hard wood floor, simply furnished, TV, WiFi, shower, central heating.', 60.00, 3, 3, 3, 4, 'Berlin', 'Spandauer str. 14', '13.401740', '52.521820', '2015-02-16 13:23:25', '2015-02-16 13:23:25', 8, 3, 2, 5),
	(8, 'Fisherman\'s Cottage', 'The Space\r\n\r\nTouchstone cottage is a lovingly restored 200 year old fishermans cottage set over three floors. It is tucked away up a secluded cobbled path a stones throw from Mevagisseys working harbour. As a 19th century building on the cusp of the harbour, you are unable to reach Touchstone Cottage by car, thus maintaining its peaceful atmosphere. \r\nThe kitchen diner on the ground floor is well equipped with electric oven and hob and dishwasher, large pine table and fridge freezer. There is a carpeted comfortable living room with a wood burning stove and the addition of a good sofa bed. There are exposed beams throughout and heaps of character. The bedroom which overlooks the cobbled back streets and bathroom are on the top floor. It has been described as beautifully quirky. Please note there is a bath but not a power shower. \r\nThis is a lovely old cottage but note that the stairs are steep and that the cobbled path leading to it can be slippery when wet! \r\nThe postcode to head for is PL26 6SP and pull up in-front of the Kit and kaboodle shop opposite the war memorial and post office you can leave your car here as you carry bags up to the cottage. You can then park in one of 3 carparks in the village or close by on Pentilley Green PL26 6TF for free and take the coastal path down to the village. \r\nWeekly car par passes are available at Willow car park.\r\n', 100.00, 2, 2, 3, 7, 'Mevagissey', 'Valey Park Ln', '-4.787462', '50.270076', '2015-02-16 13:32:50', '2015-02-16 13:33:42', 9, 1, 2, 2);
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;

-- Dumping data for table easybook.apartments_amenities: ~78 rows (approximately)
DELETE FROM `apartments_amenities`;
/*!40000 ALTER TABLE `apartments_amenities` DISABLE KEYS */;
INSERT INTO `apartments_amenities` (`id`, `amenity_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(6, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 7, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 10, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 13, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 20, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 21, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 24, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 7, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 8, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 10, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(19, 11, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(20, 14, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, 17, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, 20, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, 21, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, 24, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(25, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, 7, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, 8, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, 10, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, 14, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(32, 15, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(33, 17, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(34, 18, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(35, 21, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(36, 24, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(37, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(38, 2, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(39, 3, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(40, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(41, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(42, 6, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(43, 7, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(44, 8, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(45, 9, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(46, 12, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(47, 15, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(48, 18, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(49, 21, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(50, 24, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(51, 27, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(52, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(53, 4, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(54, 7, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(55, 10, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(56, 13, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(57, 16, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(58, 20, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(59, 21, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(60, 24, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(61, 27, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(62, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(63, 4, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(64, 5, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(65, 8, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(66, 11, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(67, 15, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(68, 18, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(69, 20, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(70, 21, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(71, 23, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(72, 24, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(73, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(74, 2, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(75, 3, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(76, 4, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(77, 5, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(78, 6, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(79, 7, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(80, 10, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(81, 13, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(82, 16, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(83, 20, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_amenities` ENABLE KEYS */;

-- Dumping data for table easybook.apartments_hostlanguages: ~28 rows (approximately)
DELETE FROM `apartments_hostlanguages`;
/*!40000 ALTER TABLE `apartments_hostlanguages` DISABLE KEYS */;
INSERT INTO `apartments_hostlanguages` (`id`, `hostlanguage_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(5, 5, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 8, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 9, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 16, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 7, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 16, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 22, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 5, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 8, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 9, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 12, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(19, 15, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(20, 18, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, 19, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, 22, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, 25, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, 2, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(25, 20, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, 9, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, 2, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, 3, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, 7, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, 22, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(32, 2, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_hostlanguages` ENABLE KEYS */;

-- Dumping data for table easybook.apartment_images: ~35 rows (approximately)
DELETE FROM `apartment_images`;
/*!40000 ALTER TABLE `apartment_images` DISABLE KEYS */;
INSERT INTO `apartment_images` (`id`, `image`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(58, 'img/apartments/2015_02_12_19_48_05_56th_st_apartment_e060810_5.jpg', 2, '2015-02-12 19:48:07', '2015-02-12 19:48:07'),
	(59, 'img/apartments/2015_02_12_19_48_06_1357717580.jpg', 2, '2015-02-12 19:48:07', '2015-02-12 19:48:07'),
	(60, 'img/apartments/2015_02_12_19_48_06_129582300588906250apartment.jpg', 2, '2015-02-12 19:48:07', '2015-02-12 19:48:07'),
	(61, 'img/apartments/2015_02_12_19_48_06_apartment-color-theme-unifies.jpg', 2, '2015-02-12 19:48:07', '2015-02-12 19:48:07'),
	(62, 'img/apartments/2015_02_12_19_48_19_apartment1.jpg', 3, '2015-02-12 19:48:21', '2015-02-12 19:48:21'),
	(63, 'img/apartments/2015_02_12_19_48_20_apartment-furniture-budget.jpg', 3, '2015-02-12 19:48:21', '2015-02-12 19:48:21'),
	(64, 'img/apartments/2015_02_12_19_48_20_Bright-colourful-Apartment-9.jpg', 3, '2015-02-12 19:48:21', '2015-02-12 19:48:21'),
	(65, 'img/apartments/2015_02_12_19_48_20_buenos-aires-short-term-apartment-studio.jpg', 3, '2015-02-12 19:48:21', '2015-02-12 19:48:21'),
	(66, 'img/apartments/2015_02_12_19_49_02_Coogee_Beach_view_from_Dolphin_Point.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(67, 'img/apartments/2015_02_12_19_49_03_feminine-apartment.jpeg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(68, 'img/apartments/2015_02_12_19_49_03_how-to-decorate-a-studio-apartment-175.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(69, 'img/apartments/2015_02_12_19_49_04_Loft-Apartment-28.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(70, 'img/apartments/2015_02_12_19_49_04_myrtle-beach-2.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(71, 'img/apartments/2015_02_12_19_49_05_nha-gia-re2 (1).jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(72, 'img/apartments/2015_02_12_19_49_05_One Bedroom Apartment 2.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(73, 'img/apartments/2015_02_12_19_49_05_Scandinavian-Apartment-white-living-entertainment-with-organic-green-and-wooden-accents.jpeg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(74, 'img/apartments/2015_02_12_19_49_06_sleek-modern-apartment-6.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(75, 'img/apartments/2015_02_12_19_49_06_Virginia_Beach_from_Fishing_Pier.jpg', 4, '2015-02-12 19:49:07', '2015-02-12 19:49:07'),
	(76, 'img/apartments/2015_02_16_13_06_21_1357717580.jpg', 5, '2015-02-16 13:06:23', '2015-02-16 13:06:23'),
	(77, 'img/apartments/2015_02_16_13_06_21_129582300588906250apartment.jpg', 5, '2015-02-16 13:06:23', '2015-02-16 13:06:23'),
	(78, 'img/apartments/2015_02_16_13_06_22_nha-gia-re2 (1).jpg', 5, '2015-02-16 13:06:23', '2015-02-16 13:06:23'),
	(79, 'img/apartments/2015_02_16_13_06_22_One bedroom Executive Apartment.jpg', 5, '2015-02-16 13:06:23', '2015-02-16 13:06:23'),
	(80, 'img/apartments/2015_02_16_13_06_22_ovolo-serviced-apartment.jpg', 5, '2015-02-16 13:06:23', '2015-02-16 13:06:23'),
	(81, 'img/apartments/2015_02_16_13_15_49_apartment-color-theme-unifies.jpg', 6, '2015-02-16 13:15:51', '2015-02-16 13:15:51'),
	(82, 'img/apartments/2015_02_16_13_15_50_Bright-colourful-Apartment-9.jpg', 6, '2015-02-16 13:15:51', '2015-02-16 13:15:51'),
	(83, 'img/apartments/2015_02_16_13_15_50_Coogee_Beach_view_from_Dolphin_Point.jpg', 6, '2015-02-16 13:15:51', '2015-02-16 13:15:51'),
	(84, 'img/apartments/2015_02_16_13_23_26_56th_st_apartment_e060810_5.jpg', 7, '2015-02-16 13:23:27', '2015-02-16 13:23:27'),
	(85, 'img/apartments/2015_02_16_13_23_26_apartment1.jpg', 7, '2015-02-16 13:23:27', '2015-02-16 13:23:27'),
	(86, 'img/apartments/2015_02_16_13_23_26_apartment-furniture-budget.jpg', 7, '2015-02-16 13:23:27', '2015-02-16 13:23:27'),
	(87, 'img/apartments/2015_02_16_13_23_27_how-to-decorate-a-studio-apartment-175.jpg', 7, '2015-02-16 13:23:27', '2015-02-16 13:23:27'),
	(88, 'img/apartments/2015_02_16_13_32_51_Loft-Apartment-28.jpg', 8, '2015-02-16 13:32:53', '2015-02-16 13:32:53'),
	(89, 'img/apartments/2015_02_16_13_32_51_myrtle-beach-2.jpg', 8, '2015-02-16 13:32:53', '2015-02-16 13:32:53'),
	(90, 'img/apartments/2015_02_16_13_32_52_One Bedroom Apartment 2.jpg', 8, '2015-02-16 13:32:53', '2015-02-16 13:32:53'),
	(91, 'img/apartments/2015_02_16_13_32_52_Scandinavian-Apartment-white-living-entertainment-with-organic-green-and-wooden-accents.jpeg', 8, '2015-02-16 13:32:53', '2015-02-16 13:32:53'),
	(92, 'img/apartments/2015_02_16_13_32_52_Virginia_Beach_from_Fishing_Pier.jpg', 8, '2015-02-16 13:32:53', '2015-02-16 13:32:53');
/*!40000 ALTER TABLE `apartment_images` ENABLE KEYS */;

-- Dumping data for table easybook.calendars: ~5 rows (approximately)
DELETE FROM `calendars`;
/*!40000 ALTER TABLE `calendars` DISABLE KEYS */;
INSERT INTO `calendars` (`id`, `apartment_id`, `date`, `from`, `to`, `information`, `note`, `status`, `created_at`, `updated_at`) VALUES
	(15, 2, NULL, '2015-02-23', '2015-02-27', '', '', 0, '2015-02-23 14:51:57', '2015-02-23 14:51:57'),
	(16, 2, NULL, '2015-02-28', '2015-03-07', '', '', 1, '2015-02-23 14:52:37', '2015-02-23 14:52:37'),
	(17, 3, NULL, '2015-02-24', '2015-02-28', '', '', 2, '2015-02-23 14:53:25', '2015-02-23 14:53:25'),
	(18, 3, NULL, '2015-03-01', '2015-03-14', '', '', 0, '2015-02-23 14:53:41', '2015-02-23 14:53:41'),
	(19, 4, NULL, '2015-02-23', '2015-03-23', '', '', 0, '2015-02-23 14:54:18', '2015-02-23 14:54:18');
/*!40000 ALTER TABLE `calendars` ENABLE KEYS */;

-- Dumping data for table easybook.comments: ~21 rows (approximately)
DELETE FROM `comments`;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `content`, `created_at`, `updated_at`, `apartment_id`, `user_id`) VALUES
	(20, 'It is very and very bad apartment!!! Nobody knows English as well, so you don\'t understand, how to talk to personal. I could advice to visit my apartment! It is clean and really beautiful.', '2015-02-16 13:10:12', '2015-02-16 13:10:12', 3, 6),
	(21, 'Pietro made sure we had a warm welcome at his home, providing some typical Italian food, together with home-made wine. Whenever we had questions or there were small problems, he was there to make sure everything worked out great. He also took some time to explain how to shoot the bow and arrow, which was pretty nice. \nHe provided us with tips of things to see in the area, like the village of Subiaco and its monastery. We also visited Rome and parked our car at the train station in Valmontone. Don\'t forget to bring some coins for the parking, which we forgot. \nThe advertisement was accurate, although the photos of the interior are a bit outdated and don\'t fully do it justice; there is more furniture to make the house cozier. \nThe area is really great, the views from the terrace are marvelous and waking up with the amazing view is really awesome.\n\nWe were really happy with our stay and Pietro was very friendly. His primary languages are Italian and French, his knowledge of English is limited. We were invited to his birthday during our stay which was also very friendly.\n\nWe will probably come again when the weather is better, we really enjoyed it!', '2015-02-16 13:11:32', '2015-02-16 13:11:32', 2, 6),
	(22, 'On a apprecié ce lieu artistique qui a une superbe vue.Accueil très chaleureux de Pietro et Pina qui habitent à coté .Pietro est très disponible, généreux et il nous a bien conseillé pour les choses à visiter et à déguster dans sa région.Notre fils a adoré jouer avec les chiens et les (email hidden)eu idéal pour rayonner aux alentours dans les montagnes et jusqu\'à Rome.Laure,Christian et Tom', '2015-02-16 13:12:11', '2015-02-16 13:12:11', 4, 6),
	(23, 'Eine wirklich einzigartige Unterkunft, mit wirklich einzigartigen Gastgebern. Als wir am späten Abend "La casa tra li ulivi" erreicht hatten lud und Pietro erst einmal zu einem wirklich leckeren Abendessen in seinen Haus ein. Und als wir dann später in das kleine Hobbit-Haus gingen, wurde unsere Erwartung einfach nur übertroffen. Es waren drei wundervolle Tage im Paradies. Danke für alles. Wir kommen wieder.', '2015-02-16 13:16:43', '2015-02-16 13:16:43', 2, 7),
	(24, 'Esperienza magnifica. Abbiamo passato un capodanno in una casa bella e particolare con un architettura davvero singolare. Ma il ringraziamento piu grande va a l\'host Pietro, una persona disponibile, gentile e premurosa che ha saputo rendere il nostro soggiorno ancora piu piacevole e rilassante, fornendoci beni dalla prima necessità fino agli snack! Sicuramente ci torneremo... grazie Pietro', '2015-02-16 13:17:09', '2015-02-16 13:17:09', 3, 7),
	(25, 'Descrivere l\'esperienza vissuta in questo magico posto non è affatto semplice. Appena arrivati, siamo stati accolti subito dalla signora Pina con un buon caffè fatto in casa. Una volta conosciuto Pietro è stato tutto in discesa, da subito in confidenza, complice anche il vino (ottimo Cesanese), grazie alla sua compagnia abbiamo passato 3 giorni bellissimi, incontrando persone incredibili e visitando luoghi fantastici. La casa non ha quasi bisogno di descrizione, come spesso accade la realtà supera le aspettative ed in questo frangente è proprio il caso di dirlo. Personalmente abbiamo molto apprezzato la cura con cui ci è stato fatto trovare l\'interno della casa, dalla fornitura degli arredi a quella della dispensa :)\n\nIn finale posso dire di aver passato davvero un bel fine settimana, nella speranza di poter tornare presto in questo luogo fantastico e passare del tempo con Pietro ed i suoi amici.\n\nRaccomando vivamente di provare l\'olio (website hidden) da Pietro e dalla sua famiglia! ECCEZIONALE!!\n\nA presto. Simone e Giulia', '2015-02-16 13:17:33', '2015-02-16 13:17:33', 4, 7),
	(26, 'In een woord FANTASTSCH. \nToen wij aankwamen werden wij hartelijk ontvangen door Pina en Pietro. We hebben koffie gedronken en kregen eigen gemaakte cake. Hierna hebben we nog een wijntje gedronken waarna we een rondleiding gekregen.\n\nHet huisje: \nSuper leuk en van alle gemakken voorzien. De tuin is schitterend en er groeien oa mandarijnen en sinaasappels die super lekker smaken. Er is een bbq waar je gebruik van kan maken. Het huis en tuin heeft veel leuke details en verschillende zit hoekjes waar je kunt genieten van het uitzicht.\n\nOmgeving: \nIn de omgeving is veel te zien. Je kunt met de auto de omgeving gaan verkennen maar als je wil kan Pietro je ook vertellen waar je zijn moet. Op 30 minuten rijden ligt Valmontone waar je de trein naar Rome kunt pakken. Je parkeert je auto daar voor 1 euro per dag! In Valmontone zit ook een pretpark en een outlet winkelcentrum. In Bellegra is een supermarkt waar je alles kunt kopen. Als je wijn wil kopen doe dit dan niet daar maar vraag aan Pietro. Wij zijn met hem mee gereden naar een wijnboer en hebben daar wijn gekocht.\n\nPietro en zijn gezin zijn geweldige mensen en geven je het gevoel als of je thuis bent! Wij hebben meerdere malen samen gegeten en dat was een fantastische ervaring. Wij zijn op veel plaatsen geweest, en mijn partner is zelf italiaans maar wij hebben zelden zo n geweldige ervaring gehad. Als je wilt weten hoe het er echt aan toe gaat in italie en als je wil weten hoe het is om een italiaan te zijn zoek dan niet verder. Dit is de plek waar je zijn wilt.\n\nWij kunnen dit iedereen aanraden en als wij weer naar deze regio gaan, gaan wij hier zeker weer naar toe!\n\nPietro, Pina. Laura en Flavia bedankt voor alles en een kus van Chiara.', '2015-02-16 13:18:30', '2015-02-16 13:18:30', 5, 7),
	(27, 'The apartment is spacious and lovely. It is extremely well equipped. \nAlex\'s girlfriend explained everything to us. Everything worked perfectly. \nIt\'s a 5 minute ride to Berlín Central Station.', '2015-02-16 13:24:17', '2015-02-16 13:24:17', 2, 5),
	(28, 'We stayed at the apartment for a week - it was wonderful! Clean, cozy, 2-minutes walk to the metro station and close to all the sights. \nAlexander and Myria were very friendly and helpful. \nWe definitely recommend this apartment!', '2015-02-16 13:24:35', '2015-02-16 13:24:35', 3, 5),
	(29, 'Die Beschreibung der Unterkunft war genau so wie beschrieben. Wir wurden freundlich empfangen und wir haben uns sehr wohl gefühlt. Die Nachbarschaft haben wir nicht kennen gelernt.', '2015-02-16 13:24:55', '2015-02-16 13:24:55', 4, 5),
	(30, 'We stayed in this lovely, spacious apartment for three nights. It was clean and as pictured, and close to public transport and suited our needs well! Alexander and Myria were lovely hosts. Very friendly and with great, prompt communication. Would happily recommend and definitely stay here again.', '2015-02-16 13:25:13', '2015-02-16 13:25:13', 5, 5),
	(31, 'Perfect and charming stay in Morgan\'s authentic fisherman\'s cottage. \nCosy and user friendly. \nGreat pubs and delicious food. \nA short jaunt to the Eden Project. \nWe loved staying and would highly recommend it.', '2015-02-16 13:35:09', '2015-02-16 13:35:09', 2, 2),
	(32, 'We had a great time in Morgan\'s beautiful little cottage. \nStunning location and everything you need for a lovely stay in a beautiful part of the world!!', '2015-02-16 13:36:23', '2015-02-16 13:36:23', 3, 2),
	(33, 'We had a great time in our short stay in Mevagissey and can thoroughly recommend Morgans cottage - it\'s very cosy and looks even better than the pictures!\n\nThe cottage was spotless when we arrived and the rooms are full of nice touches, the views from the bedroom window were great in the mornings. Mevagissey is a great place to walk round and the cottage is in a perfect location. Would definitely recommend!', '2015-02-16 13:42:57', '2015-02-16 13:42:57', 4, 2),
	(34, 'My husband and I had a lovely break at Mevagissey! Morgan was so welcoming and helpful, meeting us on our arrival and helping us to unpack. She also told us the places to visit and showed us a lovely nearby beach. The cottage was lovely, very peaceful and a complete escape from everything. Mevagissey is a very peaceful, friendly town and we enjoyed fishing on the harbour and visiting nearby attractions such as The Lost Gardens of Heligan and The Eden Project! Would highly recommend this place if you are looking for an escape!', '2015-02-16 13:43:26', '2015-02-16 13:43:26', 5, 2),
	(35, 'We had a lovely few days in Morgan\'s great house. Looks as per photos and all information sent in advance and left in the house was really useful. On arrival Morgan met us at the house, helped carry our luggage and showed us the free parking and the footpath to the local beach! Meva was really nice, quaint shops and some good options for dinner. Really glad we stayed here and not in St Austell, although would recommend the brewery tour in the town! Eden project only 10mins drive away. All in all a great few days!', '2015-02-16 13:43:58', '2015-02-16 13:43:58', 6, 2),
	(36, 'We had a lovely stay at the cottage, really cosy and felt at home,so much so i didnt want to go home! mevagissey is beautifull and the cottage is close to everything you need,would defiantly like to return one day,loved the little touches to the cottage and especially liked the different choice of books in the lounge :) found myself cosied up on the sofa with the woodburner lit on a rainy day. morgan was really nice and made us feel very welcome, Lovely little retreat ,thankyou so much ! x', '2015-02-16 13:44:23', '2015-02-16 13:44:23', 7, 2),
	(37, 'Lovely cottage, very well equipped, warm and cosy when it needed to be. Great location so close to the town, lovely view from the bedroom and Mevagissey an enchanting little place. Would highly reccomend. \nQuite expensive but probably cheaper when not in August.', '2015-02-16 13:46:15', '2015-02-16 13:46:15', 5, 4),
	(38, 'Lovely little cottage near the centre of Mevagissey, on three floors and kitchen had all you could need for a short stay in Cornwall. Didn\'t have wifi as stated (or we couldn\'t find a connection if there is wifi) but 3G sufficed. There are pay and display car parks close to the cottage, but there is free parking a short walk away. Close to the Eden project and beaches all around! Would definitely recommend!!', '2015-02-16 13:46:36', '2015-02-16 13:46:36', 6, 4),
	(39, 'Morgan was a great host! She was great with contact both before and during our stay. Her welcome was very warm; she helped us unpack and then directed us to a free car parking spot. The cream tea on arrival was a great touch. My partner and I both agreed at how lovely and interesting she was!\n\nThe cottage itself was in a great place - very close to the town and pubs etc, but remained very quite. It was also SPOTLESS.\n\nThe only downside was the lack of wifi (so no Netflix) and the shower situation was a bit tough.\n\nAside from that, it was a great cottage and Morgan was a great host!\n\n(Forgot to add that Morgan replaced a TV that was broken by previous occupant in no time at all)', '2015-02-16 13:46:54', '2015-02-16 13:46:54', 7, 4),
	(40, 'Morgans cottage is absolutly recommendable. It\'s a very pretty and cosy house in a very lovely little village! It perfectli looks like the pictures do. \nwe regrettably didn\'t meet morgan but she organized someone for the keys and explanations. \nUnfortunately the tv didn\'t work. but I\'m sure it will be fixed very fast.', '2015-02-16 13:47:16', '2015-02-16 13:47:16', 8, 4);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table easybook.countries: ~14 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'Chech Republic', 'Check', 'img/countries/2015_01_28_10_47_11_check.png', '2015-01-28 10:44:36', '2015-02-07 19:16:34'),
	(2, 'Belarus', '', 'img/countries/2015_01_28_10_47_19_belarus.png', '2015-01-28 10:45:31', '2015-01-28 10:47:19'),
	(3, 'China', '', 'img/countries/2015_01_28_10_47_31_china.png', '2015-01-28 10:47:31', '2015-01-28 10:47:31'),
	(4, 'Egypt', '', 'img/countries/2015_01_28_10_47_45_egypt.png', '2015-01-28 10:47:45', '2015-01-28 10:47:45'),
	(5, 'England', '', 'img/countries/2015_01_28_10_47_54_england.png', '2015-01-28 10:47:54', '2015-01-28 10:47:54'),
	(6, 'Finland', '', 'img/countries/2015_01_28_10_48_07_finland.png', '2015-01-28 10:48:08', '2015-01-28 10:48:08'),
	(7, 'France', '', 'img/countries/2015_01_28_10_48_15_france.png', '2015-01-28 10:48:16', '2015-01-28 10:48:16'),
	(8, 'Germany', '', 'img/countries/2015_01_28_10_48_28_germany.png', '2015-01-28 10:48:28', '2015-01-28 10:48:28'),
	(9, 'Italy', '', 'img/countries/2015_01_28_10_48_43_italy.png', '2015-01-28 10:48:43', '2015-01-28 10:48:43'),
	(10, 'Japan', '', 'img/countries/2015_01_28_10_49_09_japan.png', '2015-01-28 10:49:10', '2015-01-28 10:49:10'),
	(11, 'Netherlands', '', 'img/countries/2015_01_28_10_49_23_netherlands.png', '2015-01-28 10:49:23', '2015-01-28 10:49:23'),
	(13, 'Poland', '', 'img/countries/2015_01_28_10_49_54_poland.png', '2015-01-28 10:49:54', '2015-01-28 10:49:54'),
	(14, 'Russia', '', 'img/countries/2015_01_28_10_50_14_russia.png', '2015-01-28 10:50:14', '2015-01-28 10:50:14'),
	(15, 'USA', '', 'img/countries/2015_01_28_10_50_25_usa.png', '2015-01-28 10:50:26', '2015-01-28 10:50:26');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping data for table easybook.host_languages: ~28 rows (approximately)
DELETE FROM `host_languages`;
/*!40000 ALTER TABLE `host_languages` DISABLE KEYS */;
INSERT INTO `host_languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(2, 'English', '2014-12-29 15:56:02', '2014-12-29 15:56:02'),
	(3, 'Español', '2014-12-29 15:56:05', '2014-12-29 15:56:05'),
	(4, 'Français', '2014-12-29 15:56:09', '2014-12-29 15:56:09'),
	(5, 'Dansk', '2014-12-29 15:56:13', '2014-12-29 15:56:13'),
	(6, 'Bengali', '2014-12-29 15:56:16', '2014-12-29 15:56:16'),
	(7, 'Deutsch', '2014-12-29 15:56:20', '2014-12-29 15:56:20'),
	(8, 'Hindi', '2014-12-29 15:56:23', '2014-12-29 15:56:23'),
	(9, 'Italiano', '2014-12-29 15:56:26', '2014-12-29 15:56:26'),
	(10, 'Magyar', '2014-12-29 15:56:30', '2014-12-29 15:56:30'),
	(11, 'Nederlands', '2014-12-29 15:56:33', '2014-12-29 15:56:33'),
	(12, 'Norsk', '2014-12-29 15:56:36', '2014-12-29 15:56:36'),
	(13, 'Polski', '2014-12-29 15:56:40', '2014-12-29 15:56:40'),
	(14, 'Português', '2014-12-29 15:56:44', '2014-12-29 15:56:44'),
	(15, 'Punjabi', '2014-12-29 15:56:47', '2014-12-29 15:56:47'),
	(16, 'Suomi', '2014-12-29 15:56:51', '2014-12-29 15:56:51'),
	(17, 'Svenska', '2014-12-29 15:56:54', '2014-12-29 15:56:54'),
	(18, 'Tagalog', '2014-12-29 15:56:57', '2014-12-29 15:56:57'),
	(19, 'Türkçe', '2014-12-29 15:57:00', '2014-12-29 15:57:00'),
	(20, 'Čeština', '2014-12-29 15:57:03', '2014-12-29 15:57:03'),
	(21, 'Ελληνικά', '2014-12-29 15:57:07', '2014-12-29 15:57:07'),
	(22, 'Русский', '2014-12-29 15:57:36', '2014-12-29 15:57:36'),
	(23, 'українська', '2014-12-29 15:57:40', '2014-12-29 15:57:40'),
	(24, 'עברית', '2014-12-29 15:57:48', '2014-12-29 15:57:48'),
	(25, 'العربية', '2014-12-29 15:57:51', '2014-12-29 15:57:51'),
	(26, 'ภาษาไทย', '2014-12-29 15:57:54', '2014-12-29 15:57:54'),
	(27, '中文', '2014-12-29 15:57:58', '2014-12-29 15:57:58'),
	(28, '日本語', '2014-12-29 15:58:01', '2014-12-29 15:58:01'),
	(29, '한국어', '2014-12-29 15:58:04', '2014-12-29 15:58:04');
/*!40000 ALTER TABLE `host_languages` ENABLE KEYS */;

-- Dumping data for table easybook.migrations: ~15 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2014_12_23_113405_create_countries_table', 1),
	('2014_12_29_152442_create_roomTypes_table', 1),
	('2014_12_29_154150_create_propertyTypes_table', 1),
	('2014_12_29_155443_create_hostLanguages_table', 1),
	('2014_12_31_132133_create_roles_table', 1),
	('2014_12_31_132134_create_users_table', 1),
	('2014_12_31_132135_create_apartments_table', 1),
	('2014_12_31_132147_create_amenities_table', 1),
	('2014_12_31_133349_create_apartments_amenities_table', 1),
	('2015_01_05_095336_create_apartments_hostlanguages_table', 1),
	('2015_01_06_121652_create_apartment_images_table', 1),
	('2015_01_23_144846_create_apartment_calendars_table', 1),
	('2015_01_23_144846_create_calendars_table', 2),
	('2015_02_13_094136_create_comments_table', 2),
	('2015_02_19_165737_create_wishlist_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping data for table easybook.property_types: ~18 rows (approximately)
DELETE FROM `property_types`;
/*!40000 ALTER TABLE `property_types` DISABLE KEYS */;
INSERT INTO `property_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', '2014-12-29 15:45:07', '2014-12-29 15:45:07'),
	(2, 'House', '2014-12-29 15:45:12', '2014-12-29 15:45:12'),
	(3, 'Bed & Breakfast', '2014-12-29 15:45:21', '2014-12-29 15:45:21'),
	(4, 'Boat', '2014-12-29 15:45:30', '2014-12-29 15:45:30'),
	(5, 'Cabin', '2014-12-29 15:45:33', '2014-12-29 15:45:33'),
	(6, 'Camper/RV', '2014-12-29 15:45:36', '2014-12-29 15:45:36'),
	(7, 'Car', '2014-12-29 15:45:40', '2014-12-29 15:45:40'),
	(8, 'Castle', '2014-12-29 15:45:43', '2014-12-29 15:45:43'),
	(9, 'Cave', '2014-12-29 15:45:48', '2014-12-29 15:45:48'),
	(10, 'Chalet', '2014-12-29 15:45:53', '2014-12-29 15:45:53'),
	(11, 'Dorm', '2014-12-29 15:45:56', '2014-12-29 15:45:56'),
	(12, 'Earth House', '2014-12-29 15:46:00', '2014-12-29 15:46:00'),
	(13, 'Hut', '2014-12-29 15:46:04', '2014-12-29 15:46:04'),
	(14, 'Igloo', '2014-12-29 15:46:08', '2014-12-29 15:46:08'),
	(15, 'Island', '2014-12-29 15:46:11', '2014-12-29 15:46:11'),
	(16, 'Lighthouse', '2014-12-29 15:46:15', '2014-12-29 15:46:15'),
	(17, 'Loft', '2014-12-29 15:46:18', '2014-12-29 15:46:18'),
	(18, 'Other...', '2014-12-29 15:46:22', '2014-12-29 15:46:22');
/*!40000 ALTER TABLE `property_types` ENABLE KEYS */;

-- Dumping data for table easybook.roles: ~3 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `nicename`, `created_at`, `updated_at`) VALUES
	(1, 'SUPER_ADMIN', 'Administrator', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'LANDLORD', 'Landlord', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'TENANT', 'Tenant', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table easybook.room_types: ~3 rows (approximately)
DELETE FROM `room_types`;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Entire Place', '2014-12-29 15:35:44', '2014-12-29 15:35:44'),
	(2, 'Private Room', '2014-12-29 15:35:59', '2014-12-29 15:35:59'),
	(3, 'Shared Room', '2014-12-29 15:36:11', '2014-12-29 15:36:11');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;

-- Dumping data for table easybook.users: ~8 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `telephone`, `image`, `remember_token`, `created_at`, `updated_at`, `country_id`, `role_id`) VALUES
	(1, 'Admin', 'Adminov', 'admin@gmail.com', '$2y$10$GD.JGwANjsbzx.TepbNuCuz6K2WUY8C4YLFTQVJ53Huqt2KdToi1u', '911', 'img/users/admin.png', 'KhvxDTgdgfOorR7RFvQd8RF4hxfjgFsJDRk4jjDufj67hC6sl7zHoDN3hk29', '2015-01-28 11:05:57', '2015-02-15 16:10:11', 2, 1),
	(2, 'Dasha', 'Nikolaenko', 'nikolashaD@yandex.ru', '$2y$10$QuGpnMACWV.j5kj4Vk2xROi.kdfozx.ruFKCMOaEjC1QC53QBHfv6', '80299116609', 'img/users/2015_02_16_13_29_16_g04.png', '8LUBqGSwtc9iu2JaKi0bid77vZYpntxsHOYfZtB2pkwEH8x5scuZUaoTSNE0', '2015-01-28 11:07:33', '2015-02-16 13:44:38', 2, 2),
	(3, 'Kate', 'Lavender', 'nikolasha@mail.ru', '$2y$10$1dDkcFbVcmKoy79JT/7tc.FOLjnHFPEspdK2ueYMcuLAvl/j3iONi', '80296659854', 'img/users/2015_01_28_11_09_06_загруженное.jpg', NULL, '2015-01-28 11:09:06', '2015-01-28 11:09:06', 9, 3),
	(4, 'Dasha', 'Nikolaenko', 'nikolashaD@mail.ru', '$2y$10$yCy7R/ou0bOvJXqo6rMGu.WmTtSzYq.LFzfWtRZ.nOWDqoZc075ti', '80299116609', 'img/users/2015_02_16_13_45_22_images.jpg', 'PQyCiCXRWbQSdNNxbfTfGhQknU7w2qhKTvlssTUbaoOu570QfKEDFlK4bWqB', '2015-02-07 19:27:49', '2015-02-23 18:08:59', 14, 2),
	(5, 'Landlord', 'Landlordov', 'landlord1@email.ru', '$2y$10$KPMMXUuY9SCItVF6XyFUuuh1Ua8o8FlMdhgSIwbKt5VDYnoM58sha', '80299116609', 'img/users/2015_02_16_13_28_17_fh04.png', '4rkkTqOR4SP9kFxZpQ8LbOzxtiDGQ4ohcR5S6zxBHngXZt8oFzLec6WTurKD', '2015-02-16 12:57:28', '2015-02-16 13:28:22', 1, 2),
	(6, 'LandLord', 'LandLordov', 'landlord2@email.ru', '$2y$10$41gp0flvawglOM9R1119VeQe8TJjYfNcLYUQ460kUawerMeOgSfSO', '80293368629', 'img/users/2015_02_16_13_00_31_A7Dy18f.png', 'DZZ083oMKI2OKk1iyiySXaUnnPa8GXskZqu7N5OZWRsBvYhdOkDcgBdLxbRJ', '2015-02-16 13:00:31', '2015-02-16 13:12:39', 3, 2),
	(7, 'LandLOORD', 'LandLOORDOV', 'landlord3@email.ru', '$2y$10$5EY9Nsi4XTi/5h5DFzORrufJn.CoTGxNziszqLYH9AqQPkdNXlCzO', '80291079168', 'img/users/2015_02_16_13_01_25_a01.png', '8WJBieQFsRdN6MWBHBy7tOtKQ1jvca7TI7JFPmbeQx4UEo7SOuAWI6q7RySo', '2015-02-16 13:01:26', '2015-02-16 13:19:46', 4, 2),
	(8, 'Tenant', 'Tenantov', 'tenant1@email.ru', '$2y$10$9viyu/CihkFQ6.8tPZhqoumJ1UJL34s480T3AjnHmnRXtIuBPW3om', '80299116609', 'img/users/2015_02_16_13_51_16_mangajen.jpg', 'xyGbXISDSeXrBDhi9kcmSZT0rMWF5sCNv0D7gfeEqXrYFeZNNSRVThV7L6uL', '2015-02-16 13:51:16', '2015-02-23 14:50:36', 11, 3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table easybook.wishlists: ~4 rows (approximately)
DELETE FROM `wishlists`;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;
INSERT INTO `wishlists` (`id`, `created_at`, `updated_at`, `user_id`, `apartment_id`) VALUES
	(36, '2015-02-19 19:18:16', '2015-02-19 19:18:16', 8, 2),
	(38, '2015-02-19 19:18:22', '2015-02-19 19:18:22', 8, 6),
	(41, '2015-02-19 19:48:00', '2015-02-19 19:48:00', 8, 8),
	(42, '2015-02-19 19:57:12', '2015-02-19 19:57:12', 8, 5);
/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
