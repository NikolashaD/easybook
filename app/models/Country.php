<?php

class Country extends Eloquent {

    protected $fillable = array('name', 'description', 'image');

    public static $rules = array(
            'name' => 'required|min:3',
            'image' => 'required|image|mimes:jpg,jpeg,png,bmp,gif'
        );
    
    public function apartments(){
        return $this->hasMany('Apartment');
    }
}