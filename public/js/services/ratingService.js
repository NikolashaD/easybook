angular.module('ratingService', [])

.factory('Rating', function($http) {

    return {
        get : function(apartmentId) {
            return $http.get('/main/apartments/'+apartmentId+'/rating');
        },
        save : function(ratingData) {
            return $http({
                method: 'POST',
                url: '/main/apartments/rating',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(ratingData)
            });
        },

    }

});