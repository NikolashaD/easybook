@extends('layouts.dashboard')

@section('content')
<div id="admin">
    <h1>Your Profile Page</h1>

    <p>Here you can view all the profile details.</p>

    <table class="profile">
    	<tr>
    		<td>       
	    		@if($errors->has())
	        		<div >
		            <p class="label label-danger"> The following errors have occurred: </p>
		            <ul>
		                @foreach($errors->all() as $error)
		                <li class="red">{{ $error }}</li>
		                @endforeach
		            </ul>
		        	</div>
		        @endif  
	    	</td>
    	</tr>
    	{{ Form::open(array('url'=>'users/update','files'=>true, 'class'=>'form-signin')) }}
    	<tr>
    		<td>{{ HTML::image(Auth::user()->image, Auth::user()->firstname,  array('class'=>'img-circle', 'height'=>170, 'width'=>170)) }}</td>
    	</tr>
    	<tr>
    		<td>{{ Form::file('image') }} </td>
    	</tr>    	
    	<tr>
    		<td><h3>Personal Info</h3></td>
    	</tr>  
    	<tr>
    		<td>{{ Form::label('Email address')}} {{ Form::text('email', Auth::user()->email, array('class'=>'form-control')) }}</td>
    	</tr>  
    	<tr>
    		<td>{{ Form::label('Firstname')}} {{ Form::text('firstname', Auth::user()->firstname, array('class'=>'form-control')) }}</td>
    	</tr>    
    	<tr>
    		<td>{{ Form::label('Lastname')}} {{ Form::text('lastname', Auth::user()->lastname, array('class'=>'form-control')) }}</td>
    	</tr>     	
    	<tr>
    		<td>{{ Form::label('Telephone number')}} {{ Form::text('telephone', Auth::user()->telephone, array('class'=>'form-control')) }}</td>
    	</tr>   
    	<tr>
    		<td>{{ Form::label('Country')}} {{ Form::select('country_id', $countries, Auth::user()->country_id, array('class'=>'form-control')) }}</td>
    	</tr>   
    	<tr>
    		<td><br /><div style="float:right;">{{ Form::hidden('id', Auth::user()->id) }}{{ Form::submit('Save', array('class'=>'btn btn-success')) }}</div></td>
    	</tr>      	

    	{{ Form::close() }}      
    </table>

    <hr />
</div>

@stop