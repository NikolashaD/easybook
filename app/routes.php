<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses'=>'HomeController@getIndex'));

/* SITE SECTION */

Route::controller('users', 'UsersController');


/* ADMIN SECTION */

Route::controller('admin/', 'DashboardController');
Route::controller('admin/dashboard', 'DashboardController');

Route::controller('admin/countries', 'CountriesController');

Route::controller('admin/amenities', 'AmenitiesController');

Route::controller('admin/roomTypes', 'RoomTypesController');

Route::controller('admin/propertyTypes', 'PropertyTypesController');

Route::controller('admin/hostLanguages', 'HostLanguagesController');

Route::controller('admin/calendars', 'CalendarsController');

Route::controller('admin/apartments', 'ApartmentsController');

Route::get('main/apartments/', array('uses'=>'ApartmentsController@getApartmentsByFilterData'));

Route::get('main/apartments/{id?}', array('uses'=>'ApartmentsController@getApartments'));

Route::post('main/apartments/comments', array('uses'=>'CommentsController@postComment'));

Route::get('main/apartments/{id?}/comments', array('uses'=>'CommentsController@getComments'));

Route::delete('main/apartments/comments/{id?}', array('uses'=>'CommentsController@destroyComment'));

Route::controller('admin/wishlist', 'WishlistController');

Route::post('main/apartments/wishlist', array('uses'=>'WishlistController@addToWishlist'));

Route::delete('main/apartments/wishlist', array('uses'=>'WishlistController@removeFromWishlist'));

Route::post('main/apartments/rating', array('uses'=>'RatingController@postRating'));

Route::get('main/apartments/{id?}/rating', array('uses'=>'RatingController@getRating'));