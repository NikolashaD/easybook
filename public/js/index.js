//$("form#my-awesome-dropzone").dropzone({ url: "/file/post" });

$(document).on('click','li',function(e){
    $('li').removeClass('active');
});

$(document).on('click','thead',function(){
    if($(this).hasClass('jquery-actions')){
        first_part_of_url = window.location.href;
        second_part_of_url = '/delete-selected';
        url = first_part_of_url + second_part_of_url;
        url = '/admin/countries/delete-selected/';
        
        $checked = $('tr').find('div.btn-group>label.active');
        
        var ids = []; 
        $checked.each(function(ind, val){
            ids.push($(val).find('i').attr('data-id'));
        });

        $.ajax({
            type: "GET",
            url: url,
            async: false,
            data: {ids: ids},
            success : function(data) {
                if(data.success){
                    location.reload();
                }
            }
        });

        return false;        
    }    
    
    if($(this).closest('table').find('tbody').hasClass('hide')){
       $(this).closest('table').find('tbody').removeClass('hide');
    }else{
       $(this).closest('table').find('tbody').toggle(); 
    }       
});

$(document).on('click','input',function(e){
    e.stopPropagation(); 
});
$(document).on('click','a',function(e){
    e.stopPropagation(); 
});
$(document).on('click','select',function(e){
    e.stopPropagation(); 
});


$(document).on('click','div',function(e){
    e.stopPropagation(); 
    if($(this).hasClass('select-all')){
        if($(this).hasClass('active')){
            $('tr').find('div.btn-group').removeClass('active');
            $('tr').find('div.btn-group>label').removeClass('active');            
        }else{
            $('tr').find('div.btn-group').addClass('active');
            $('tr').find('div.btn-group>label').addClass('active');
        }
    }
});

$(document).on('click','tr',function(event){
    if($(this).hasClass('show-grey')){
        if($(this).next().next().next().hasClass('hide')){
            $(this).nextUntil('.show-grey').removeClass('hide');
        }else{
            $(this).nextUntil('.show-grey').addClass('hide');
        }       
    }      
});

$(document).on('click','tr',function(event){
    if($(this).hasClass('show-grey-simple')){
        if($(this).next().hasClass('hide')){
            $(this).nextUntil('.show-grey-simple').removeClass('hide');
        }else{
            $(this).nextUntil('.show-grey-simple').addClass('hide');
        }       
    }      
});


$(document).on('click','a#view-calendar',function(event){
  $( "#datepicker" ).datepicker();    
  $( "#datepicker" ).show();    
});
$(document).on('click','a#hide-calendar',function(event){
  $( "#datepicker" ).hide();    
});
$(document).on('click','a.open-popup',function(event){
  $('#light').css('display', 'block');
  $('#fade').css('display','block'); 
});
$(document).on('click','a.close-popup',function(event){
  $('#light').css('display', 'none');
  $('#fade').css('display','none');  
});



function openPopup(){
  console.log('open popup');
  date = $(this).text();
  month = $(this).parent().attr('data-month') + 1;
  year = $(this).parent().attr('data-year');

  showPopup();

}



$(document).on('click','button#add-to-wishlist',function(event){
        first_part_of_url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1];
        second_part_of_url = '/apartments/wishlist';
        url = first_part_of_url + second_part_of_url;
        
        userId = $(this).attr('data-userId');
        apartmentId = $(this).attr('data-apartmentId');

        $.ajax({
            type: "POST",
            url: url,
            async: false,
            data: {user_id: userId, apartment_id:apartmentId},
            success : function(data) {
                if(data.success){
                    console.log(data.success);
                    $('button#add-to-wishlist > span').text(' Remove from Wish list');
                    $('button#add-to-wishlist > i').attr('class', 'glyphicon glyphicon-trash');
                    $('button#add-to-wishlist').attr('id', 'remove-from-wishlist');
                }
            }
        });

        return false;  
});

$(document).on('click','button#remove-from-wishlist',function(event){
        first_part_of_url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1];
        second_part_of_url = '/apartments/wishlist';
        url = first_part_of_url + second_part_of_url;
        
        userId = $(this).attr('data-userId');
        apartmentId = $(this).attr('data-apartmentId');

        $.ajax({
            type: "DELETE",
            url: url,
            async: false,
            data: {user_id: userId, apartment_id:apartmentId},
            success : function(data) {
                if(data.success){
                    console.log(data.success);
                    $('button#remove-from-wishlist > span').text(' Add to Wish list');
                    $('button#remove-from-wishlist > i').attr('class', 'glyphicon glyphicon-heart');
                    $('button#remove-from-wishlist').attr('id', 'add-to-wishlist');
                }
            }
        });

        return false;  
});