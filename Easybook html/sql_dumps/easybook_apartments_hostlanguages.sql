-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:14:28
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.apartments_hostlanguages
DROP TABLE IF EXISTS `apartments_hostlanguages`;
CREATE TABLE IF NOT EXISTS `apartments_hostlanguages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostlanguage_id` int(10) unsigned NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartments_hostlanguages_hostlanguage_id_index` (`hostlanguage_id`),
  KEY `apartments_hostlanguages_apartment_id_index` (`apartment_id`),
  CONSTRAINT `apartments_hostlanguages_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `apartments_hostlanguages_hostlanguage_id_foreign` FOREIGN KEY (`hostlanguage_id`) REFERENCES `host_languages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartments_hostlanguages: ~4 rows (approximately)
DELETE FROM `apartments_hostlanguages`;
/*!40000 ALTER TABLE `apartments_hostlanguages` DISABLE KEYS */;
INSERT INTO `apartments_hostlanguages` (`id`, `hostlanguage_id`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 22, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 22, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `apartments_hostlanguages` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
