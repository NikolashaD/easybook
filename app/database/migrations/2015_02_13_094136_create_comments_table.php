<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('comments', function(Blueprint $table)
        {
            $table->increments('id');    
            $table->string('content');
    
            $table->timestamps();

            $table->integer('apartment_id')->unsigned();
            $table->integer('user_id')->unsigned();            
        });

        Schema::table('comments', function($table){                
            $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade')->onUpdate('cascade');             
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');              
        });        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
