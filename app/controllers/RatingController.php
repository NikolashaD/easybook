<?php

class RatingController extends BaseController{

    public function __construct(){
        //$this->beforefilter('csrf', array('on'=>'post'));
    }

    public function index()
    {
        return Response::json(Rating::get());
    }    

    public function postRating(){        
        $currentUser = Auth::user() ? Auth::user()->id : null;

        // TODO: add filter if user has already rated this apartment

        if(Rating::where('user_id', '=', $currentUser)->first()){
            $rating = Rating::where('user_id', '=', $currentUser)->first();
        }else{
            $rating = new Rating();
        }
        $rating->points = Input::get('points');
        $rating->apartment_id = Input::get('apartment_id');
        $rating->user_id = Input::get('user_id');
        $rating->save();

        return Response::json(array('success' => true));
    }

    public function getRating($id = null){
        $currentUser = Auth::user() ? Auth::user()->id : null;

        $ratingSum = Rating::where('apartment_id', '=', $id)->sum('points');
        $ratingAmmount = Rating::where('apartment_id', '=', $id)->count();

        $avarage = round($ratingSum / $ratingAmmount);

        $myVote = Rating::where('apartment_id', '=', $id)
                        ->where('user_id', '=', $currentUser)
                        ->sum('points');

        $rating = array(
            'colors'=> array(
                array('ind' => 1, 'isPaint' => (1 <= $avarage) ? true : false ),
                array('ind' => 2, 'isPaint' => (2 <= $avarage) ? true : false ),
                array('ind' => 3, 'isPaint' => (3 <= $avarage) ? true : false ),
                array('ind' => 4, 'isPaint' => (4 <= $avarage) ? true : false ),
                array('ind' => 5, 'isPaint' => (5 <= $avarage) ? true : false ),
            ),
            'myVotes'=> array(
                array('ind' => 1, 'isPaint' => (1 <= $myVote) ? true : false ),
                array('ind' => 2, 'isPaint' => (2 <= $myVote) ? true : false ),
                array('ind' => 3, 'isPaint' => (3 <= $myVote) ? true : false ),
                array('ind' => 4, 'isPaint' => (4 <= $myVote) ? true : false ),
                array('ind' => 5, 'isPaint' => (5 <= $myVote) ? true : false ),
                        ),        
            'voters' => $ratingAmmount
        );

        return Response::json($rating);
    }    
}