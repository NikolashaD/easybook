var ratingApp = angular.module('ratingApp', ['ratingCtrl', 'ratingService'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

var commentApp = angular.module('commentApp', ['mainCtrl', 'commentService'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

angular.element(document).ready(function() {
  var ratingModule = document.getElementById("ratingModule");
  angular.bootstrap(ratingModule, ["ratingApp"]);

  var commentModule = document.getElementById("commentModule");
  angular.bootstrap(commentModule, ["commentApp"]);
});