<?php

class AmenitiesController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/amenities');
    }

    public function getIndex() {
        return View::make('amenities.index')
                    ->with('amenities', Amenity::all());
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), Amenity::$rules);

        if($validator->passes()){
            $amenity = new Amenity();
            $this->saveAmenity($amenity);

            return Redirect::to('admin/amenities/index')
                        ->with('message', 'New amenity has been successfully created!');
        }

        return Redirect::to('admin/amenities/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $amenity = Amenity::find(Input::get('id'));

        if($amenity){
            $this->saveAmenity($amenity);
            return Redirect::to('admin/amenities/index')
                ->with('message', 'Amenity Updated!');
        }

        return Redirect::to('admin/amenities/index')
            ->with('message', 'Something went wrong, please, try again');
    }       

    public function postDestroy() {
        $amenity = Amenity::find(Input::get('id'));

        if($amenity){
            $amenity->delete();

            return Redirect::to('admin/amenities/index')
                ->with('message', 'Amenity Deleted!');
        }

        return Redirect::to('admin/amenities/index')
            ->with('message', 'Something went wrong, please, try again');
    }




    private function saveAmenity($amenity){
        $amenity->name = Input::get('name');
        
        if($amenity->save()){
            return true;
        }

        return false;
    }

}