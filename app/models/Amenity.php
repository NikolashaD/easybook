<?php

class Amenity extends Eloquent {

    protected $fillable = array('name');

    public static $rules = array('name' => 'required|min:2');
    
    public function apartments(){
        return $this->belongsToMany('Apartment', 'apartments_amenities', 'amenity_id', 'apartment_id');
    }   
}