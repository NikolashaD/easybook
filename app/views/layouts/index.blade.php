<!DOCTYPE html>
<html>
<head>
    <title>EasyBook Home Page</title>

    <meta http-equiv = "content-type" content = "text/html; charset = UTF-8">
    <link rel="icon" href={{ asset('img/favicon.png') }}>

    {{ HTML::script('js/jquery-2.1.1.min.js') }}
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/docs.min.js') }}
    {{ HTML::script('js/plugins/tcal.js') }}

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-theme.min.css') }}    
    {{ HTML::style('css/index.css') }}
    {{ HTML::style('css/carousel.css') }}
    {{ HTML::style('css/plugins/tcal.css') }}
</head>

<body style="background-color: #edefed;">

@yield('search')


<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            {{ HTML::image('img/pic/landing1.jpg', 'First slide')}}
            <div class="container">
                <div class="carousel-caption">
                    <h1>Welcome to EasyBook service</h1>
                    <p><a class="btn btn-lg btn-success" href="#" role="button">How It Works</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            {{ HTML::image('img/pic/landing2.jpg', 'Second slide')}}            
            <div class="container">
                <div class="carousel-caption">
                    <h1>Another example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-success" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            {{ HTML::image('img/pic/landing3.jpg', 'Third slide')}}      
            <div class="container">
                <div class="carousel-caption">
                    <h1>One more for good measure.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-success" href="#" role="button">Browse gallery</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->        



@if(Session::has('message'))
    <div class="alert alert-success" role="alert">
        <strong>{{ Session::get('message') }}</strong>
    </div>
@endif

@yield('content')



<br /><br /><br />
<!-- FOOTER -->
<footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>

</div><!-- /.container -->    

</body>

</html>