-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:15:28
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.property_types
DROP TABLE IF EXISTS `property_types`;
CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.property_types: ~18 rows (approximately)
DELETE FROM `property_types`;
/*!40000 ALTER TABLE `property_types` DISABLE KEYS */;
INSERT INTO `property_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Apartment', '2014-12-29 15:45:07', '2014-12-29 15:45:07'),
	(2, 'House', '2014-12-29 15:45:12', '2014-12-29 15:45:12'),
	(3, 'Bed & Breakfast', '2014-12-29 15:45:21', '2014-12-29 15:45:21'),
	(4, 'Boat', '2014-12-29 15:45:30', '2014-12-29 15:45:30'),
	(5, 'Cabin', '2014-12-29 15:45:33', '2014-12-29 15:45:33'),
	(6, 'Camper/RV', '2014-12-29 15:45:36', '2014-12-29 15:45:36'),
	(7, 'Car', '2014-12-29 15:45:40', '2014-12-29 15:45:40'),
	(8, 'Castle', '2014-12-29 15:45:43', '2014-12-29 15:45:43'),
	(9, 'Cave', '2014-12-29 15:45:48', '2014-12-29 15:45:48'),
	(10, 'Chalet', '2014-12-29 15:45:53', '2014-12-29 15:45:53'),
	(11, 'Dorm', '2014-12-29 15:45:56', '2014-12-29 15:45:56'),
	(12, 'Earth House', '2014-12-29 15:46:00', '2014-12-29 15:46:00'),
	(13, 'Hut', '2014-12-29 15:46:04', '2014-12-29 15:46:04'),
	(14, 'Igloo', '2014-12-29 15:46:08', '2014-12-29 15:46:08'),
	(15, 'Island', '2014-12-29 15:46:11', '2014-12-29 15:46:11'),
	(16, 'Lighthouse', '2014-12-29 15:46:15', '2014-12-29 15:46:15'),
	(17, 'Loft', '2014-12-29 15:46:18', '2014-12-29 15:46:18'),
	(18, 'Other...', '2014-12-29 15:46:22', '2014-12-29 15:46:22');
/*!40000 ALTER TABLE `property_types` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
