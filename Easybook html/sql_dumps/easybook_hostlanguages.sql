-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:15:16
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.host_languages
DROP TABLE IF EXISTS `host_languages`;
CREATE TABLE IF NOT EXISTS `host_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.host_languages: ~28 rows (approximately)
DELETE FROM `host_languages`;
/*!40000 ALTER TABLE `host_languages` DISABLE KEYS */;
INSERT INTO `host_languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(2, 'English', '2014-12-29 15:56:02', '2014-12-29 15:56:02'),
	(3, 'Español', '2014-12-29 15:56:05', '2014-12-29 15:56:05'),
	(4, 'Français', '2014-12-29 15:56:09', '2014-12-29 15:56:09'),
	(5, 'Dansk', '2014-12-29 15:56:13', '2014-12-29 15:56:13'),
	(6, 'Bengali', '2014-12-29 15:56:16', '2014-12-29 15:56:16'),
	(7, 'Deutsch', '2014-12-29 15:56:20', '2014-12-29 15:56:20'),
	(8, 'Hindi', '2014-12-29 15:56:23', '2014-12-29 15:56:23'),
	(9, 'Italiano', '2014-12-29 15:56:26', '2014-12-29 15:56:26'),
	(10, 'Magyar', '2014-12-29 15:56:30', '2014-12-29 15:56:30'),
	(11, 'Nederlands', '2014-12-29 15:56:33', '2014-12-29 15:56:33'),
	(12, 'Norsk', '2014-12-29 15:56:36', '2014-12-29 15:56:36'),
	(13, 'Polski', '2014-12-29 15:56:40', '2014-12-29 15:56:40'),
	(14, 'Português', '2014-12-29 15:56:44', '2014-12-29 15:56:44'),
	(15, 'Punjabi', '2014-12-29 15:56:47', '2014-12-29 15:56:47'),
	(16, 'Suomi', '2014-12-29 15:56:51', '2014-12-29 15:56:51'),
	(17, 'Svenska', '2014-12-29 15:56:54', '2014-12-29 15:56:54'),
	(18, 'Tagalog', '2014-12-29 15:56:57', '2014-12-29 15:56:57'),
	(19, 'Türkçe', '2014-12-29 15:57:00', '2014-12-29 15:57:00'),
	(20, 'Čeština', '2014-12-29 15:57:03', '2014-12-29 15:57:03'),
	(21, 'Ελληνικά', '2014-12-29 15:57:07', '2014-12-29 15:57:07'),
	(22, 'Русский', '2014-12-29 15:57:36', '2014-12-29 15:57:36'),
	(23, 'українська', '2014-12-29 15:57:40', '2014-12-29 15:57:40'),
	(24, 'עברית', '2014-12-29 15:57:48', '2014-12-29 15:57:48'),
	(25, 'العربية', '2014-12-29 15:57:51', '2014-12-29 15:57:51'),
	(26, 'ภาษาไทย', '2014-12-29 15:57:54', '2014-12-29 15:57:54'),
	(27, '中文', '2014-12-29 15:57:58', '2014-12-29 15:57:58'),
	(28, '日本語', '2014-12-29 15:58:01', '2014-12-29 15:58:01'),
	(29, '한국어', '2014-12-29 15:58:04', '2014-12-29 15:58:04');
/*!40000 ALTER TABLE `host_languages` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
