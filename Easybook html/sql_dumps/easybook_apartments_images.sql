-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:14:50
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.apartment_images
DROP TABLE IF EXISTS `apartment_images`;
CREATE TABLE IF NOT EXISTS `apartment_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `apartment_images_apartment_id_foreign` (`apartment_id`),
  CONSTRAINT `apartment_images_apartment_id_foreign` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.apartment_images: ~7 rows (approximately)
DELETE FROM `apartment_images`;
/*!40000 ALTER TABLE `apartment_images` DISABLE KEYS */;
INSERT INTO `apartment_images` (`id`, `image`, `apartment_id`, `created_at`, `updated_at`) VALUES
	(1, 'img/apartments/2015_01_28_11_01_42_pic-1.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(2, 'img/apartments/2015_01_28_11_01_42_pic-2.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(3, 'img/apartments/2015_01_28_11_01_42_pic-3.jpg', 1, '2015-01-28 11:01:42', '2015-01-28 11:01:42'),
	(4, 'img/apartments/2015_01_28_11_02_55_pic-1.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(5, 'img/apartments/2015_01_28_11_02_55_pic-2.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(6, 'img/apartments/2015_01_28_11_02_55_pic-3 - Copy.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55'),
	(7, 'img/apartments/2015_01_28_11_02_55_pic-3.jpg', 2, '2015-01-28 11:02:55', '2015-01-28 11:02:55');
/*!40000 ALTER TABLE `apartment_images` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
