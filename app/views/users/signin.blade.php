@extends('layouts.main')

@section('content')
    {{ Form::open(array('url'=>'users/signin','class'=>'form-signin')) }}
        <h2 class="form-signin-heading">Please fill all the information</h2>

        @if($errors->has())
        <div >
            <p class="label label-danger"> The following errors have occurred: </p>
            <ul>
                @foreach($errors->all() as $error)
                <li class="red">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif  
        {{ Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'Email address')) }}
        {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}     
        <br />
        {{ Form::submit('Sign in', array('class'=>'btn btn-lg btn-success btn-block')) }}
    {{ Form::close() }}    

@stop