<?php

class HomeController extends BaseController {

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
    }

    public function getIndex() {
        $countries = array();     
        $countries[0] = 'Country';   
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }

        return View::make('home.index')
        	->with('countries', Country::take(12)->orderByRaw("RAND()")->get())
        	->with('filterCountries', $countries)
        	;        
    }

}
