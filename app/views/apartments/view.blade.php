@extends('layouts.main')

@section('content')
{{ HTML::style('css/apartment-view.css') }}

<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize() {
        var point = new google.maps.LatLng({{ $apartment->latitude }},{{ $apartment->longitude }});

              var mapProp = {
                center: point,
                zoom:15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
              };
              var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var marker = new google.maps.Marker({
            position: point,
            map: map,
            title:"Hello World!"
        });      
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    var apartmentId = {{ $apartment->id }};
</script>
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    	@foreach($apartment->apartmentImages as $key=>$image)
    		@if($key == 0)
    			<li data-target="#myCarousel" data-slide-to="{{ $key }}" class="active"></li>
    		@else
    			<li data-target="#myCarousel" data-slide-to="{{ $key }}"></li>
    		@endif
        @endforeach
    </ol>
    <div class="carousel-inner" role="listbox">
    	@foreach($apartment->apartmentImages as $key=>$image)
    		@if($key == 0)
		        <div class="item active">
		            {{ HTML::image($image->image, 'slide')}}
		        </div>
    		@else
		        <div class="item">
		            {{ HTML::image($image->image, 'slide')}}
		        </div>
    		@endif
        @endforeach    
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->   

<h1 style="padding-left:42%;">{{ $apartment->name }}</h1>
<div style="background-color: white;" class="container marketing">
    <hr>

    <div class="middle">
        <div class="wraper">
            <div class="small-block-1">
                {{ HTML::image($apartment->user->image, $apartment->user->firstname,  array('class'=>'img-circle', 'height'=>170, 'width'=>160)) }}
            </div>

            <div class="small-block-2" id="ratingModule" ng-controller="ratingController">   
                <div style="float:left; width:40%;">            
                    <div class="rat" ng-repeat="color in rating.colors">
                        <span id="rat-<% color.ind %>" class="glyphicon glyphicon-star-empty" ng-if="color.isPaint" style="color:green;"></span>
                        <span id="rat-<% color.ind %>" class="glyphicon glyphicon-star-empty" ng-if="!color.isPaint"></span>
                    </div>
                    <span class="summury">(<% rating.voters %>)</span>
                </div>
                @if(Auth::user())
                    <form style="float:right;" ng-submit="submitRating()">                    
                        <input type="hidden" class="form-control" name="user_id" ng-model="ratingData.user_id" ng-init="ratingData.user_id  = {{ Auth::user()->id }}" />  
                        <input type="hidden" class="form-control" name="apartment_id" ng-model="ratingData.apartment_id" ng-init="ratingData.apartment_id  = {{ $apartment->id }}" />
                        <div class="rating">
                            <input ng-repeat="index in list" ng-if="rating.myVotes[index-1].isPaint" type="image" src={{ URL::asset('img/star.png') }} name="points" ng-model="ratingData.points" ng-click="ratingData.points  = index" id="rating-<% index %>" class="rating-star" />
                            <input ng-repeat="index in list" ng-if="!rating.myVotes[index-1].isPaint" type="image" src={{ URL::asset('img/black-star.png') }} name="points" ng-model="ratingData.points" ng-click="ratingData.points  = index" id="rating-<% index %>" class="rating-star" />                            
                        </div>
                    </form>
                @endif
                <br /><br />
                <div>
                    <span style="font-size:20px;">{{ $apartment->country->name }}, {{ $apartment->city }}, {{ $apartment->address }}.</span>
                    <br />
                    <span class="label label-success">Price: {{ $apartment->price }} $ per day</span>
                </div>
                <br />
                <div>
                    <span>{{ $apartment->description}}</span>
                </div>                
            </div>        

            <div class="small-block-3">
            <div class="price"><span class="big-white">$<b>{{ $apartment->price }}</b></span> <span class="small-grey">for the entire period</span> </div>         
            <table class="table">
                <tr>
                    <td>Check In</td>
                    <td>Check Out</td>
                    <td>Guests</td>
                </tr>   
                <tr>
                    <td>2015/02/02</td>
                    <td>2015/03/03</td>
                    <td>4</td>
                </tr>    
                @if(Auth::user()) 
                    <div class="hide"> 
                        {{ $isInWishlist = false }}
                        @foreach(Auth::user()->wishlist as $k=>$wish)
                            @if($wish->apartment_id == $apartment->id)
                                {{ $isInWishlist = true }}
                            @endif
                        @endforeach
                    </div>
                    <tr>
                        <td colspan="3"><button id="send-request" data-userId = "{{ Auth::user()->id }}" data-apartmentId = "{{ $apartment->id }}" type="button" class="btn btn-success btn-lg btn-block"><i class="glyphicon glyphicon-ok"></i><span> Request to Book</span></button></td>
                    </tr>
                    @if($isInWishlist)                 
                        <tr>
                            <td colspan="3"><button id="remove-from-wishlist" data-userId = "{{ Auth::user()->id }}" data-apartmentId = "{{ $apartment->id }}" type="button" class="btn btn-default btn-lg btn-block"><i class="glyphicon glyphicon-trash"></i><span> Remove from Wish list</span></button></td>
                        </tr> 
                    @else
                        <tr>
                            <td colspan="3"><button id="add-to-wishlist" data-userId = "{{ Auth::user()->id }}" data-apartmentId = "{{ $apartment->id }}" type="button" class="btn btn-default btn-lg btn-block"><i class="glyphicon glyphicon-heart"></i><span> Add to Wish list</span></button></td>
                        </tr>                      
                    @endif
                @else
                    <tr>
                        <td colspan="3"><button type="button" class="btn btn-default btn-lg active"><i class="glyphicon glyphicon-ok"></i> Request to Book</button></td>
                    </tr> 
                    <tr>
                        <td colspan="3"><button type="button" class="btn btn-default btn-lg active"><i class="glyphicon glyphicon-heart"></i> Add to Wish list</button></td>
                    </tr>
                @endif
            </table>
            </div>    
        </div> 

        <div class="map">
            <div id="googleMap" style="width:95%;height:380px;"></div>
        </div>


        <div class="info">
            <div class="apartmen-info">
                <table class="table">
                    <tbody>
                        <div class="hide"> 
                            @foreach($apartment->amenities as $ka=>$a)
                                {{ $apartmentAmenities[$ka] = $a->id; }}
                            @endforeach
                            <br />
                            @foreach($apartment->hostLanguages as $kh=>$h)
                                {{ $apartmentHostlanguages[$kh] = $h->id; }}
                            @endforeach  
                            <br /><br />
                        </div>
                            <tr>
                                <td></td>                
                                <td>{{ Form::label('Location') }}</td>                  
                                <td>
                                    {{ Form::label('Country') }}
                                    <br/> 
                                    {{ $apartment->country->name }}  
                                </td>
                                <td>
                                    {{ Form::label('Room type') }} 
                                    <br/> 
                                    {{ $apartment->roomType->name }} 
                                </td>                    
                                <td>
                                    {{ Form::label('Property type') }} 
                                    <br/> 
                                    {{ $apartment->propertyType->name }}
                                </td>               
                            </tr>  
                            <tr>
                                <td></td>                
                                <td>{{ Form::label('Size') }}</td>                  
                                <td>
                                    {{ Form::label('Bedrooms') }} 
                                    <br />
                                    {{ $apartment->bedrooms+1 }}
                                </td>
                                <td>
                                    {{ Form::label('Bathrooms') }} 
                                    <br />
                                    {{ $apartment->bathrooms+1 }}
                                </td>
                                <td>
                                    {{ Form::label('Beds') }} 
                                    <br />
                                    {{ $apartment->beds+1 }}
                                </td>                
                            </tr> 
                                @for ($i = 0; $i < count($amenities); $i+=3)     
                                <tr>
                                    <td></td>
                                    <td>
                                        @if($i == 0)
                                            {{ Form::label('Amenities') }}
                                        @endif
                                    </td>
                                    <td>  
                                        @if(isset($amenities[$i]))
                                            {{ Form::checkbox('amenities[]', $amenities[$i]->id, in_array($amenities[$i]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                            {{ $amenities[$i]->name }} 
                                        @endif
                                    </td>                        
                                    <td>     
                                        @if(isset($amenities[$i+1]))
                                            {{ Form::checkbox('amenities[]', $amenities[$i+1]->id, in_array($amenities[$i+1]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                            {{ $amenities[$i+1]->name }} 
                                        @endif
                                    </td>
                                    <td>    
                                        @if(isset($amenities[$i+2]))
                                            {{ Form::checkbox('amenities[]', $amenities[$i+2]->id, in_array($amenities[$i+2]->id, $apartmentAmenities), array('disabled'=>'disabled')); }}
                                            {{ $amenities[$i+2]->name }} 
                                        @endif
                                    </td> 
                                </tr>   
                                @endfor   

                                @for ($i = 0; $i < count($hostLanguages); $i+=3)                 
                                <tr>
                                    <td></td> 
                                    <td>
                                        @if($i == 0)
                                            {{ Form::label('Host languages') }}
                                        @endif
                                    </td>
                                    <td>  
                                        @if(isset($hostLanguages[$i]))
                                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i]->id, in_array($hostLanguages[$i]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                            {{ $hostLanguages[$i]->name }} 
                                        @endif
                                    </td>                        
                                    <td>     
                                        @if(isset($hostLanguages[$i+1]))
                                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+1]->id, in_array($hostLanguages[$i+1]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                            {{ $hostLanguages[$i+1]->name }} 
                                        @endif
                                    </td>
                                    <td>    
                                        @if(isset($hostLanguages[$i+2]))
                                            {{ Form::checkbox('hostLanguages[]', $hostLanguages[$i+2]->id, in_array($hostLanguages[$i+2]->id, $apartmentHostlanguages), array('disabled'=>'disabled')); }}
                                            {{ $hostLanguages[$i+2]->name }} 
                                        @endif
                                    </td>
                                </tr>   
                                @endfor                 
          
                    </tbody>
                </table>               
            </div>
            <div class="landlord-info">
            <table class="table">
                <tr class="green">
                    <th colspan="2"><span style="color:white;">Landlord Information :</span></th>
                </tr>   
                <tr>
                    <th>Name</th>
                    <td>{{ $apartment->user->firstname }} {{ $apartment->user->lastname }}</td>
                </tr> 
                <tr>
                    <th>Telephone</th>
                    <td>{{ $apartment->user->telephone }}</td>
                </tr>    
                <tr>
                    <th>Email</th>
                    <td>{{ $apartment->user->email }}</td>
                </tr> 
                <tr class="green">
                    <th colspan="2"><span style="color:white;">About:</span></th>
                </tr> 
                <tr>
                    <th>School</th>
                    <td>ssh #189</td>
                </tr> 
                <tr>
                    <th>Work</th>
                    <td>programmer</td>
                </tr>  
                <tr>
                    <th>Languages</th>
                    <td>english, russian</td>
                </tr> 
                <tr>
                    <td colspan="2"><button type="button" class="btn btn-default btn-lg btn-block"><i class="glyphicon glyphicon-envelope"></i> Send an Email</button></td>
                </tr>    
            </table>                
            </div>
     
        </div>  

        <!-- Comments Block -->
        <div class="comments" id="commentModule" ng-controller="mainController">
            <div class="col-md-8">
            @if(Auth::user())
                <form ng-submit="submitComment()">
                    <div class="avatar">
                        {{ HTML::image(Auth::user()->image, '', array('class'=>'img-circle')) }}
                    </div>
                    <div class="form-group">
                        <textarea rows="5" class="form-control" name="content" ng-model="commentData.content" placeholder="Say what you have to say"> </textarea>
                        <input type="hidden" class="form-control" name="apartment_id" ng-model="commentData.apartment_id" ng-init="commentData.apartment_id = {{ $apartment->id }}" />
                        <input type="hidden" class="form-control" name="user_id" ng-model="commentData.user_id" ng-init="commentData.user_id  = {{ Auth::user()->id }}" />  
                        <br />       
                        <button style="float:right;" type="submit" class="btn btn-success btn-lg">Submit</button>
                    </div>
                </form>
            @endif

                <p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>


                <div class="comment" ng-hide="loading" ng-repeat="comment in comments">
                    <div class="avatar">
                        <img src="{{ URL::to('/'); }}/<% comment.avatar %>" class="img-circle" />                    
                    </div> 
                    <div style="float:left; width:80%;">               
                        <h3>Comment # <% comment.ind %></h3> <span class="label label-success">by <% comment.author %> <% comment.date %></span>
                        <br /><br />
                        <p><% comment.content %></p>
                        <p ng-if="comment.isOwner"><a href="#comment-block" ng-click="deleteComment(comment.id)" class="text-muted">Delete</a></p>
                    </div>
                    
                </div>
      
                
            </div>             
        </div>
        <!-- End Comments Block -->
    </div>

@stop