<?php

class UsersController extends BaseController{

    public function __construct(){
    	//parent::__construct();
        $this->beforefilter('csrf', array('on'=>'post'));
    }


    public function getSignup(){
        $countries = array();        
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }

        $roles = array();        
        foreach(Role::all() as $role){
            if($role->name != 'SUPER_ADMIN'){
                $roles[$role->id] = $role->nicename;
            }
        }


		return View::make('users.signup')
                ->with('countries', $countries)
                ->with('roles', $roles);
    }

    public function postSignup(){
    	$validator = Validator::make(Input::all(), User::$rules);

        if($validator->passes()){
        	$user = new User();
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->telephone = Input::get('telephone');

            $image = Input::file('image');   
            $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
            $path = public_path('img/users/' . $filename);
            Image::make($image->getRealPath())->resize(468,249)->save($path);
            
            $user->image = 'img/users/'.$filename;   
            
            $user->country_id = Input::get('country_id');
            $user->role_id = Input::get('role_id');

            $user->save();

	        return Redirect::to('users/signin')
	        	->with('message', 'You have been successfully registered! Please, sign in.');
        }
        return Redirect::to('users/signup')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();        

    }


    public function getSignin(){
		return View::make('users.signin');
    }

    public function postSignin(){
    	if(Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))){
	        return Redirect::to('/')
	        	->with('message', 'Thanks for singing in.');    		
    	}
        return Redirect::to('users/signin')
                    ->with('message', 'Your email/password combo was incorrect.');       	
    }

    public function getSignout(){
    	Auth::logout();
        return Redirect::to('users/signin')
                    ->with('message', 'You have been signed out.');  
    }


    public function getProfile(){
        $countries = array();        
        foreach(Country::orderBy('name')->get() as $country){
            $countries[$country->id] = $country->name;
        }        
        return View::make('users.profile')
                ->with('countries', $countries);
    }


    public function postUpdate(){
        $user = User::find(Input::get('id'));

        if($user){
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');   
            $user->email = Input::get('email');   
            $user->telephone = Input::get('telephone');   
            
            $user->country_id = Input::get('country_id');    

            $image = Input::file('image');   
            if($image){                  
                $filename = date('Y_m_d_H_i_s').'_'.$image->getClientOriginalName();
                $path = public_path('img/users/' . $filename);
                Image::make($image->getRealPath())->resize(468,249)->save($path);

                $user->image = 'img/users/'.$filename;   
            }          
    
            $user->save();    
            
            return Redirect::to('users/profile')
                ->with('message', 'User Updated!');
        }

        return Redirect::to('users/profile')
            ->with('message', 'Something went wrong, please, try again');
    }

}