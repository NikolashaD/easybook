<?php

class PropertyType extends Eloquent {

    protected $fillable = array('name');

    public static $rules = array('name' => 'required|min:3');
    
    public function apartments(){
        return $this->hasMany('Apartment');
    }    
}