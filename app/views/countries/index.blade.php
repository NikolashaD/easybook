@extends('layouts.dashboard')

@section('content')
<div id="admin">
    <h1>Countries Admin Panel</h1>

    <p>Here you can view, delete, and create new countries.</p>

    <hr />
    @if($errors->has())
    <div >
        <p class="label label-danger"> The following errors have occurred: </p>
        <ul>
            @foreach($errors->all() as $error)
            <li class="red">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
  
    {{ Form::open(array('url'=>'admin/countries/create', 'files'=>true)) }}
    <div class="form-inline">

        <div class="form-group" style="width:100%;">
            <table class="table table-hover">
            <thead>
                <tr class="grey">
                    <td width="250px"><h4>{{ Form::label('Info') }}</h4></td>
                    <td  width="429px" colspan="2">{{ Form::label('Title') }} {{ Form::text('name', '', array('class'=>'form-control')) }}</td>                    
                    <td>{{ Form::label('Image', 'Choose an image') }} {{ Form::file('image') }}</td>                                      
                </tr>
            </thead>
            <tbody class="hide">
                <tr class="grey">
                    <td><h4>{{ Form::label('Description') }}</h4></td>
                    <td colspan="3">{{ Form::textarea('description', '', array('class'=>'form-control')) }}</td>
                </tr>               
            </tbody>                
            </table>
        </div>
        <br />
        <div style="float:left; width:50%;">{{ Form::submit('Create', array('class'=>'btn btn-success')) }}</div>
        <div style="float:right;">{{ Form::submit('Create', array('class'=>'btn btn-success')) }}</div>

    </div>
    {{ Form::close() }}
    <br /><br />
    <h2>Countries</h2>
    <hr />

    <table class="table table-striped">
        <thead>
            <tr>
                <th width="5%">№</th>
                <th width="18%">Image</th>
                <th width="10%">Name</th>
                <th width="57%">Description</th>
                <th width="5%">Action</th>
                <th width="5%">
                    <div class="btn-group select-all" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="checkbox" autocomplete="off"><i class="glyphicon glyphicon-ok glyphicon-white"></i>
                        </label>
                    </div>                    
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($countries as $key=>$country)
            <tr class="show-grey-simple">
                <td>
                    {{ $key + 1 }}
                </td>         
                <td>
                    {{ HTML::image($country->image, $country->name, array('width'=>'150', 'height'=>'150')) }}
                </td>                  
                <td>
                    <h4>{{ $country->name }}</h4>
                </td>
                <td>
                    {{ implode(' ', array_slice(explode(' ', $country->description), 0, 150))  }} ...
                </td>                
                <td>
                    {{ Form::open(array('url'=>'admin/countries/destroy', 'class'=>'form-inline')) }}
                    {{ Form::hidden('id', $country->id) }}
                    {{ Form::submit('delete', array('class'=>'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
                <td>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="checkbox" autocomplete="off"><i data-id="{{$country->id}}" class="glyphicon glyphicon-ok glyphicon-white"></i>
                        </label>
                    </div>
                </td>
            </tr>
            <tr class="hide show-white">
                <td>{{ Form::open(array('url'=>'admin/countries/update','files'=>true, 'class'=>'form-inline')) }}</td>
                <td>{{ Form::label('Image', 'Choose an image') }} {{ Form::file('image') }}</td>
                <td>{{ Form::text('name', $country->name, array('class'=>'form-control')) }}</td>
                <td>{{ Form::textarea('description', $country->description, array('class'=>'form-control')) }}</td>
                <td>                        
                    {{ Form::hidden('id', $country->id) }}
                    {{ Form::submit('save', array('class'=>'btn btn-success')) }}
                    {{ Form::close() }}
                </td>
                <td></td>
            </tr>     
            @endforeach
        </tbody>
    </table>
    <hr />
    <table class="table table-striped">
        <thead class="jquery-actions">
            <tr>
                <th width="86%"></th>
                <th width="9%"><button type="button" class="btn btn-danger">Delete selected</button>  </th>
                <th>                    
                    <div class="btn-group select-all" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="checkbox" autocomplete="off"><i class="glyphicon glyphicon-ok glyphicon-white"></i>
                        </label>
                    </div>                     
                </th>
            </tr>
        </thead>
    </table>

</div>
<br /><br />

@stop