<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsHostlanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('apartments_hostlanguages', function(Blueprint $table){
                $table->increments('id')->unsigned();
                $table->integer('hostlanguage_id')->unsigned()->index();
                $table->foreign('hostlanguage_id')->references('id')->on('host_languages')->onDelete('cascade');
                $table->integer('apartment_id')->unsigned()->index();
                $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade');
                $table->timestamps();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('apartments_hostlanguages');
	}

}
