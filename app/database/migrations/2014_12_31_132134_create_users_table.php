<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function($table){
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->string('password');
            $table->string('telephone');
            $table->string('image');   
            $table->text('remember_token')->nullable(); 
            $table->timestamps();                
            
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('role_id')->unsigned()->nullable();
        });
        
        
        Schema::table('users', function($table){                
            $table->foreign('country_id')->references('id')->on('countries')->onDelete(DB::raw('set null'));             
            $table->foreign('role_id')->references('id')->on('roles')->onDelete(DB::raw('set null'));              
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
