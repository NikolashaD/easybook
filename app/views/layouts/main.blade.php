<!DOCTYPE html>
<html>
<head>
    <title>EasyBook</title>
    <link rel="icon" href={{ asset('img/favicon.png') }}>
    <meta http-equiv = "content-type" content = "text/html; charset = UTF-8">


    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/docs.min.js') }}
    {{ HTML::script('js/plugins/angular.js') }}
    {{ HTML::script('js/plugins/tcal.js') }}
    {{ HTML::script('js/index.js') }}

    {{ HTML::script('js/controllers/mainCtrl.js') }}
    {{ HTML::script('js/services/commentService.js') }}

    {{ HTML::script('js/controllers/ratingCtrl.js') }}
    {{ HTML::script('js/services/ratingService.js') }}

    {{ HTML::script('js/app.js') }}

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('css/bootstrap-theme.min.css') }}
    {{ HTML::style('css/dashboard.css') }}
    {{ HTML::style('css/plugins/tcal.css') }}
    {{ HTML::style('css/main.css')}}
</head>


<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="logo navbar-brand" href="/">{{ HTML::image('img/logo.png', 'EasyBook', array('class'=>'logo')) }}<b>&nbsp;&nbsp; EasyBooooook</b></a>			  
			</div>	

			@if(Auth::check())
				<div id="navbar" class="navbar-collapse collapse">
				  <form class="navbar-form navbar-right" role="form">
					<div class="form-group">						
					<div class="btn-group">
					  <button type="button" class="btn btn-default">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</button>
					  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					  </button>
					  <ul class="dropdown-menu" role="menu">
						<li>{{ HTML::link('admin/dashboard', 'Dashboard')}}</li>
						<li>{{ HTML::link('users/signout', 'Sign Out')}}</li>
					  </ul>
					</div>		
				  </form>
				</div>
			@else
				<div id="navbar" class="navbar-collapse collapse">
			      {{ Form::open(array('url'=>'users/signin','class'=>'navbar-form navbar-right')) }}
						<div class="form-group">
						  {{ Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'Email address')) }}
						</div>
						<div class="form-group">
						  {{ Form::password('password', array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Password')) }}
						</div>
						<div class="btn-group">
						  {{ Form::submit('Sign in', array('class'=>'btn btn-default')) }}
						  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<li>{{ HTML::link('users/signup', 'Sign Up')}}</li>
						  </ul>
						</div>		
				    {{ Form::close() }}  
				</div>
			@endif


		</div>
    </nav>

    <div class="container">


    @if(Session::has('message'))
    	<br />
        <div class="alert alert-success" role="alert">
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif

    @yield('content')

    </div>


<footer>
	<p>&copy; Company 2014</p>		
</footer>	 
</body>
</html>