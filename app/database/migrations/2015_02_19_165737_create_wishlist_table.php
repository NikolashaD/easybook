<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishlistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('wishlists', function(Blueprint $table)
        {
            $table->increments('id');    
            $table->timestamps();
            
            $table->integer('user_id')->unsigned();            
            $table->integer('apartment_id')->unsigned();
        });

        Schema::table('wishlists', function($table){          
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');     
            $table->foreign('apartment_id')->references('id')->on('apartments')->onDelete('cascade')->onUpdate('cascade');                  
        }); 
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wishlists');
	}

}
