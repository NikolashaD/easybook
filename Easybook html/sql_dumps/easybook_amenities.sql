-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-01-28 14:13:51
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table easybook.amenities
DROP TABLE IF EXISTS `amenities`;
CREATE TABLE IF NOT EXISTS `amenities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table easybook.amenities: ~26 rows (approximately)
DELETE FROM `amenities`;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Wireless Internet', '2014-12-29 15:11:23', '2014-12-29 15:11:23'),
	(2, 'TV', '2014-12-29 15:11:47', '2014-12-29 15:11:47'),
	(3, 'Kitchen', '2014-12-29 15:11:55', '2014-12-29 15:11:55'),
	(4, 'Air Conditioning', '2014-12-29 15:12:04', '2014-12-29 15:12:04'),
	(5, 'Breakfast', '2014-12-29 15:12:08', '2014-12-29 15:12:08'),
	(6, 'Cable TV', '2014-12-29 15:12:11', '2014-12-29 15:12:11'),
	(7, 'Doorman', '2014-12-29 15:12:15', '2014-12-29 15:12:15'),
	(8, 'Dryer', '2014-12-29 15:12:19', '2014-12-29 15:12:19'),
	(9, 'Elevator in Building', '2014-12-29 15:12:23', '2014-12-29 15:12:23'),
	(10, 'Essentials', '2014-12-29 15:12:27', '2014-12-29 15:12:27'),
	(11, 'Family/Kid Friendly', '2014-12-29 15:12:31', '2014-12-29 15:12:31'),
	(12, 'Fire Extinguisher', '2014-12-29 15:12:34', '2014-12-29 15:12:34'),
	(13, 'First Aid Kit', '2014-12-29 15:12:38', '2014-12-29 15:12:38'),
	(14, 'Gym', '2014-12-29 15:12:43', '2014-12-29 15:12:43'),
	(15, 'Heating', '2014-12-29 15:12:46', '2014-12-29 15:12:46'),
	(16, 'Hot Tub', '2014-12-29 15:12:49', '2014-12-29 15:12:49'),
	(17, 'Indoor Fireplace', '2014-12-29 15:12:52', '2014-12-29 15:12:52'),
	(18, 'Internet', '2014-12-29 15:12:57', '2014-12-29 15:12:57'),
	(19, 'Pets Allowed', '2014-12-29 15:13:02', '2014-12-29 15:13:02'),
	(20, 'Pool', '2014-12-29 15:13:05', '2014-12-29 15:13:05'),
	(21, 'Safety Card', '2014-12-29 15:13:08', '2014-12-29 15:13:08'),
	(22, 'Shampoo', '2014-12-29 15:13:12', '2014-12-29 15:13:12'),
	(23, 'Smoke Detector', '2014-12-29 15:13:16', '2014-12-29 15:13:16'),
	(24, 'Smoking Allowed', '2014-12-29 15:13:19', '2014-12-29 15:13:19'),
	(25, 'Suitable for Events', '2014-12-29 15:13:23', '2014-12-29 15:13:23'),
	(26, 'Washer', '2014-12-29 15:13:27', '2014-12-29 15:13:27');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
