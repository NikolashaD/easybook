<?php

class RoomTypesController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/roomTypes');          
    }

    public function getIndex() {
        return View::make('roomTypes.index')
                    ->with('roomTypes', RoomType::all());
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), RoomType::$rules);

        if($validator->passes()){
            $roomType = new RoomType();
            $this->saveRoomType($roomType);

            return Redirect::to('admin/roomTypes/index')
                        ->with('message', 'New room type has been successfully created!');
        }

        return Redirect::to('admin/roomTypes/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $roomType = RoomType::find(Input::get('id'));

        if($roomType){
            $this->saveRoomType($roomType);

            return Redirect::to('admin/roomTypes/index')
                ->with('message', 'Room Type Updated!');
        }

        return Redirect::to('admin/roomTypes/index')
            ->with('message', 'Something went wrong, please, try again');
    }       

    public function postDestroy() {
        $roomType = RoomType::find(Input::get('id'));

        if($roomType){
            $roomType->delete();

            return Redirect::to('admin/roomTypes/index')
                ->with('message', 'Room Type Deleted!');
        }

        return Redirect::to('admin/roomTypes/index')
            ->with('message', 'Something went wrong, please, try again');
    }


    private function saveRoomType($roomType){
        $roomType->name = Input::get('name');
        if($roomType->save()){
            return true;
        }        

        return false;
    }

}