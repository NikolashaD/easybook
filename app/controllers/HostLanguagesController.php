<?php

class HostLanguagesController extends BaseController{

    public function __construct(){
        $this->beforefilter('csrf', array('on'=>'post'));
        $this->beforefilter('admin');
        $this->beforefilter('admin/hostLanguages');        
    }

    public function getIndex() {
        return View::make('hostLanguages.index')
                    ->with('hostLanguages', HostLanguage::all());
    }

    public function postCreate() {
        $validator = Validator::make(Input::all(), HostLanguage::$rules);

        if($validator->passes()){
            $hostLanguage = new HostLanguage();
            $this->saveHostLanguage($hostLanguage);

            return Redirect::to('admin/hostLanguages/index')
                        ->with('message', 'New host language has been successfully created!');
        }

        return Redirect::to('admin/hostLanguages/index')
                    ->with('message', 'Something went wrong')
                    ->withErrors($validator)
                    ->withInput();
    }
    
    public function postUpdate() {
        $hostLanguage = HostLanguage::find(Input::get('id'));

        if($hostLanguage){
            $this->saveHostLanguage($hostLanguage);

            return Redirect::to('admin/hostLanguages/index')
                ->with('message', 'Host language Updated!');
        }

        return Redirect::to('admin/hostLanguages/index')
            ->with('message', 'Something went wrong, please, try again');
    }          

    public function postDestroy() {
        $hostLanguage = HostLanguage::find(Input::get('id'));

        if($hostLanguage){
            $hostLanguage->delete();

            return Redirect::to('admin/hostLanguages/index')
                ->with('message', 'Host Language Deleted!');
        }

        return Redirect::to('admin/hostLanguages/index')
            ->with('message', 'Something went wrong, please, try again');
    }



    private function saveHostLanguage($hostLanguage){
        $hostLanguage->name = Input::get('name');
        
        if($hostLanguage->save()){
            return true;
        }        

        return false;
    }

}