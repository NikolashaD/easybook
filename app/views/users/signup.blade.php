@extends('layouts.main')

@section('content')


    {{ Form::open(array('url'=>'users/signup','files'=>true, 'class'=>'form-signin')) }}
        <h2 class="form-signin-heading">Please fill all the information</h2>

        @if($errors->has())
        <div >
            <p class="label label-danger"> The following errors have occurred: </p>
            <ul>
                @foreach($errors->all() as $error)
                <li class="red">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif  

        {{ Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'Email address')) }}
        {{ Form::password('password', array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Password')) }}  
        {{ Form::password('password_confirmation', array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Password confirmation')) }}  
        <hr />        
        {{ Form::text('firstname', '', array('class'=>'form-control', 'placeholder'=>'First name')) }}
        {{ Form::text('lastname', '', array('class'=>'form-control', 'placeholder'=>'Last name')) }}
        {{ Form::text('telephone', '', array('class'=>'form-control', 'placeholder'=>'Telephone number')) }}
        {{ Form::select('country_id', $countries,'', array('class'=>'form-control')) }}
        {{ Form::select('role_id', $roles,'', array('class'=>'form-control')) }}
        {{ Form::label('Image', 'Choose images') }}
        {{ Form::file('image') }}      
        <br />
        {{ Form::submit('Sign up', array('class'=>'btn btn-lg btn-success btn-block')) }}
    {{ Form::close() }}      

@stop