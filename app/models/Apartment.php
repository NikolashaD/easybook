<?php

class Apartment extends Eloquent {

    protected $fillable = array('name', 'desctription', 'price', 'bedrooms', 'bathrooms', 'beds', 'guests',
        'city', 'address', 'longitude', 'latitude', 
        'country_id', 'room_type_id', 'property_type_id', 'user_id');

    public static $rules = array(
        'name' => 'required|min:2',
        'description' => 'required|min:10',
        'price' => 'required|numeric',
        'bedrooms' => 'required|integer',
        'bathrooms' => 'required|integer',
        'beds' => 'required|integer',
        'guests' => 'required|integer',
        'city' => 'required|min:2',
        'address' => 'required|min:5',
        'longitude' => 'required|min:2',
        'latitude' => 'required|min:2',
        'image' => 'image|mimes:jpg,jpeg,png,bmp,gif',
        'country_id' => 'required|integer',
        'room_type_id' => 'required|integer',        
        'property_type_id' => 'required|integer',
        'user_id' => 'required|integer',
    );
    
    public function country(){
        return $this->belongsTo('Country');
    }
    
    public function roomType(){
        return $this->belongsTo('RoomType');
    }
    
    public function propertyType(){
        return $this->belongsTo('PropertyType');
    }   
    
   
    public function amenities(){
        return $this->belongsToMany('Amenity', 'apartments_amenities', 'apartment_id', 'amenity_id');
    }    
    
    public function hostLanguages(){
        return $this->belongsToMany('HostLanguage', 'apartments_hostlanguages', 'apartment_id', 'hostlanguage_id');
    }     
    
    public function apartmentImages(){
        return $this->hasMany('ApartmentImage');    
    }    

    public function calendars(){
        return $this->hasMany('Calendar');
    }    

    public function user(){
        return $this->belongsTo('User');
    }

    public function comments(){
        return $this->hasMany('Comment');
    }    

    public function wishlist(){
        return $this->hasMany('Wishlist');
    }

    public function rating(){
        return $this->hasMany('Rating');
    }                     
}
