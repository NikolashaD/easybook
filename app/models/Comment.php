<?php

class Comment extends Eloquent {

    protected $fillable = array('content', 'apartment_id', 'user_id');

    public static $rules = array(
        'content' => 'required|min:10',       
        'apartment_id' => 'required|integer',
        'user_id' => 'required|integer',
    );
    
    public function apartment(){
        return $this->belongsTo('Apartment');
    }  

    public function user(){
        return $this->belongsTo('User');
    }    
}
