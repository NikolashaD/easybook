<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = array('firstname', 'lastname', 'email', 'telephone', 'image', 'country_id', 'role_id');	

    public static $rules = array(
        'firstname' => 'required|min:2|alpha',
        'lastname' => 'required|min:2||alpha',
        'password' => 'required|alpha_num||between:8,12|confirmed',
        'password_confirmation' => 'required|alpha_num||between:8,12',
        'email' => 'required|email|unique:users',
        'telephone' => 'required|between:9,12',
        'image' => 'required|image|mimes:jpg,jpeg,png,bmp,gif',

        'country_id' => 'required|integer',
        'role_id' => 'required|integer'
    );	


	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function country(){
        return $this->belongsTo('Country');
    }
	public function role(){
        return $this->belongsTo('Role');
    }    

    public function apartments(){
        return $this->hasMany('Apartment');
    }

    public function comments(){
        return $this->hasMany('Comment');
    }    

    public function wishlist(){
        return $this->hasMany('Wishlist');
    }     

    public function rating(){
        return $this->hasMany('Rating');
    }       
}
